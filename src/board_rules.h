/*--------------------------------------------------------------------------*/
/* game board rules                                                         */
/*--------------------------------------------------------------------------*/

#ifndef __MY_BOARD_RULES_H
#define __MY_BOARD_RULES_H

/*--------------------------------------------------------------------------*/
/* private structures                                                       */
/*--------------------------------------------------------------------------*/

typedef struct route_search_node {
   int x, y;                                /* location to visit */
   struct route_search_node *next;
} ROUTE_SEARCH_NODE;


typedef struct {
   ROUTE_SEARCH_NODE *front;
   ROUTE_SEARCH_NODE *back;
} ROUTE_SEARCH_QUEUE;

typedef struct {
   int ax0, ay0;                            /* location of the cached actor */
   int dist[BOARD_YCELLS][BOARD_XCELLS];    /* travel distances             */
   int dir[BOARD_YCELLS][BOARD_XCELLS];     /* how we arrived               */
} BOARD_ROUTE_CACHE;

/*--------------------------------------------------------------------------*/
/* public structures                                                        */
/*--------------------------------------------------------------------------*/

typedef union {                         /* computer command */
   struct {
      int spell;                        /* spell, or 0 for simple move */
      int ax0, ay0;                     /* source cell (move, spell caster) */
      int sx0, sy0;                     /* spell source (unused for el)     */
      int x1, y1;                       /* target cell (move, t/x/el)       */
   } b;
   struct {
      int dir;                          /* direction of command */
      int fire;                         /* command modifier: fire or move */
   } f;
} COMMAND;

typedef struct {
   int flags;                           /* see CELL_* #define's above */
   ACTOR *actor;                        /* actor occupying cell */
   void *rocks;                         /* rocks placement for field mode */
} CELL;

typedef struct {
   int turn;                            /* count of turns since game start */
   int side;                            /* 0=light,  1=dark */
   int winner;                          /* see enum in board.h */
   int lumi, lumi_d;                    /* luminance value and direction */
   int cell_w, cell_h;                  /* # of cells wide/high board is */
   int game;                            /* 0 for Archon, 1 for Adept... */
   int spell_avails[2][SPELL_COUNT_2];  /* row 0 is light, row 1 is dark */
   CELL cells[BOARD_YCELLS][BOARD_XCELLS];
   CELL elem;
   BOARD_ROUTE_CACHE routes;            /* routes&distances of ground actor */
} BOARD_STATE;

/*--------------------------------------------------------------------------*/
/* public functions                                                         */
/*--------------------------------------------------------------------------*/

void board_rules_init(BOARD_STATE *state);
void board_rules_copy(BOARD_STATE *state, BOARD_STATE *orig);
void board_rules_clear(BOARD_STATE *state);
int board_rules_side(BOARD_STATE *state);
int board_rules_turn(BOARD_STATE *state);
int board_rules_lumi(BOARD_STATE *state);
int board_rules_lumi_d(BOARD_STATE *state);
CELL * board_rules_cell(BOARD_STATE *state, int x, int y);
CELL * board_rules_field_cell(BOARD_STATE *state, COMMAND cmd);
int board_rules_cell_lumi(BOARD_STATE *state, int x, int y);
void board_rules_cell_init(BOARD_STATE *state, int x, int y, int actor_num);
void board_rules_cell_clear(BOARD_STATE *state, int x, int y);
ACTOR * board_rules_actor(BOARD_STATE *state, int x, int y);
ACTOR * board_rules_elem_actor(BOARD_STATE *state);
ACTOR * board_rules_attacker(BOARD_STATE *state, COMMAND cmd);
ACTOR * board_rules_defender(BOARD_STATE *state, COMMAND cmd);
int board_rules_actor_side(BOARD_STATE *state, int x, int y);
int board_rules_actor_range(BOARD_STATE *state, int x, int y);
int board_rules_actor_is_master(BOARD_STATE *state, int x, int y);
int board_rules_find_actor(BOARD_STATE *state, int type, int *ax, int *ay);
int board_rules_elem_num(BOARD_STATE *state, int elem_num);
int board_rules_power_point(BOARD_STATE *state, int x, int y);
int board_rules_cell_animated(BOARD_STATE *state, int x, int y);
int board_rules_empty(BOARD_STATE *state, int x, int y);
int board_rules_width(BOARD_STATE *state);
int board_rules_height(BOARD_STATE *state);
int board_rules_in_range(BOARD_STATE *state, int x, int y);
int board_rules_imprisoned(BOARD_STATE *state, int x, int y);

void board_rules_start_game(BOARD_STATE *state, int game, int light_first);

int board_rules_make_move(BOARD_STATE *state, COMMAND cmd);
int board_rules_field_winner(BOARD_STATE *state, COMMAND cmd, 
                             int field_result);
int board_rules_winner(BOARD_STATE *state);

int board_rules_verify_move(BOARD_STATE *state, COMMAND cmd);
int board_rules_verify_source(BOARD_STATE *state, int x0, int y0);
int board_rules_verify_actor_step(BOARD_STATE *state, int x0, int y0, 
				  int x1, int y1, int dir);
int board_rules_verify_actor_target(BOARD_STATE *state, 
				    int x0, int y0, int x1, int y1);
int board_rules_verify_spell(BOARD_STATE *state, int x, int y, int spell);
int board_rules_verify_spell_source(BOARD_STATE *state, 
				    int spell, int x, int y);
int board_rules_verify_spell_target(BOARD_STATE *state, 
				    int spell, int x0, int y0, 
				    int sx0, int sy0, int x1, int y1);

int board_rules_in_revive_target_range(BOARD_STATE *state, 
				       int master_x, int master_y, 
				       int x, int y);
int board_rules_route_exists(BOARD_STATE *state, 
			     int x0, int y0, int x1, int y1);
int board_rules_next_actor_step(BOARD_STATE *state, int ax, int ay, 
                                int x0, int y0, int x1, int y1);

int board_rules_spell_count(BOARD_STATE *state);
int board_rules_spell_choice(BOARD_STATE *state, int choice);
int board_rules_spell_index(BOARD_STATE *state, int spell);
int board_rules_revive_count(BOARD_STATE *state);
int board_rules_revive_choice(BOARD_STATE *state, int count);

/* add these public functions? */
int board_rules_spell_available(BOARD_STATE *state, int side, int spell);
/* int board_rules_verify_imprison_source(BOARD_STATE *state, int x, int y); */

/*--------------------------------------------------------------------------*/
/* private functions                                                         */
/*--------------------------------------------------------------------------*/

void board_rules_cast_heal(BOARD_STATE *state, int x, int y);
void board_rules_cast_shift_time(BOARD_STATE *state);
void board_rules_cast_exchange(BOARD_STATE *state, 
			       int x0, int y0, int x1, int y1);
void board_rules_cast_revive(BOARD_STATE *state, int y0, int x1, int y1);
void board_rules_cast_imprison(BOARD_STATE *state, int x, int y);
void board_rules_move_actor(BOARD_STATE *state, 
			    int x0, int y0, int x1, int y1);
int board_rules_end_turn(BOARD_STATE *state);
int board_rules_check_win(BOARD_STATE *state);

int board_rules_verify_revive(BOARD_STATE *state, int master_x, int master_y);
int board_rules_verify_imprison(BOARD_STATE *state);

int board_rules_verify_revive_source(BOARD_STATE *state, int revive_num);

int board_rules_verify_teleport_target(BOARD_STATE * state, 
				       int x0, int y0, int x1, int y1);
int board_rules_verify_exchange_target(BOARD_STATE * state, int x1, int y1);
int board_rules_verify_elemental_target(BOARD_STATE * state, 
					int x0, int y0, int x1, int y1);
int board_rules_verify_monster_target(BOARD_STATE *state, 
				      int x0, int y0, int x1, int y1);
int board_rules_verify_revive_target(BOARD_STATE *state,
				     int x0, int y0, int x1, int y1);
int board_rules_revive_targets(BOARD_STATE *state, int master_x, int master_y);
int board_rules_can_revive_actor(BOARD_STATE *state, int actor_num);
int board_rules_legal_revive_target(BOARD_STATE *state, 
				    int master_x, int master_y, int x, int y);

int board_rules_next_ground_step(BOARD_STATE *state, int ax, int ay,
                                 int x0, int y0, int x1, int y1);
int board_rules_in_fly_range(BOARD_STATE *state, int ax, int ay, 
			     int x1, int y1);

void board_rules_calc_ground_routes(BOARD_STATE *state, int x0, int y0);
void board_rules_init_ground_routes(BOARD_STATE *state, int x0, int y0);
void board_rules_search_ground_routes(BOARD_STATE *state);
void board_rules_finalize_ground_routes(BOARD_STATE *state);

/*--------------------------------------------------------------------------*/
/* methods for CELL                                                         */
/*--------------------------------------------------------------------------*/

void cell_init(CELL *cell, int actor_num);
void cell_copy(CELL *cell, CELL *orig);
int cell_lumi(CELL *cell, int board_lumi);
void cell_clear(CELL *cell);

/*--------------------------------------------------------------------------*/
/* methods for COMMAND                                                      */
/*--------------------------------------------------------------------------*/

void command_copy(COMMAND *cmd, COMMAND orig);

/*--------------------------------------------------------------------------*/
/* methods for ROUTE_SEARCH_QUEUE                                           */
/*--------------------------------------------------------------------------*/

void route_search_queue_init(ROUTE_SEARCH_QUEUE *q);
void route_search_queue_clear(ROUTE_SEARCH_QUEUE *q);
void route_search_queue_push(ROUTE_SEARCH_QUEUE *q, int x, int y);
int route_search_queue_shift(ROUTE_SEARCH_QUEUE *q, int *x_ref, int *y_ref);

#endif /* __MY_BOARD_RULES_H */
