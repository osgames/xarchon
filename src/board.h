/*--------------------------------------------------------------------------*/
/* game board                                                               */
/*--------------------------------------------------------------------------*/

#ifndef __MY_BOARD_H
#define __MY_BOARD_H

#include "actors.h"
#include "board_const.h"
#include "board_rules.h"
#include "board_canvas.h"

/*--------------------------------------------------------------------------*/
/* private constants                                                        */
/*--------------------------------------------------------------------------*/

/* select_what: what input are we waiting for? */
enum {
   SELECT_SOURCE = 1,                   /* non-spell */
   SELECT_TARGET,
   SELECT_FIELD,                        /* field mode */
   SELECT_GAME_OVER,                    /* the state to end all states! */
   SELECT_SPELL,
   SELECT_SPELL_SOURCE,
   SELECT_SPELL_TARGET
};

/* cursor movement styles */
enum {
   CMOVE_ANIMATE,
   CMOVE_STEP
};

/* cursor appearance */
enum {
   CURSOR_BOX,
   CURSOR_ACTOR,
   CURSOR_MESSAGE
};

enum {
   NOT_READY_FOR_UPDATE,
   READY_FOR_UPDATE
};

/*--------------------------------------------------------------------------*/
/* structures                                                               */
/*--------------------------------------------------------------------------*/

typedef struct {
   int fire_down;                       /* fire key is held */
   int dir_held;                        /* what key is held (0 if none) */
   /* need some more info about prev and next? */
} BOARD_IFACE;

typedef struct {
   BOARD_STATE state;                   /* info about the last complete turn */
   BOARD_IFACE iface;                   /* input source */
   BOARD_CANVAS canvas;                 /* output target */
   int game_status;                     /* 0=none,  1=active,  -1=paused */
   int select_what;                     /* what part of the turn we're in */
   int action_result;                   /* result code of the last action */
   int cx, cy;                          /* current cursor position */
   int cstate;                          /* state of cursor movement */
   int cmove;                           /* style of cursor movement */
   COMMAND cmd;                         /* command being constructed */
   int frame_time;
   char *side_name[2];                  /* light/order and dark/chaos */
} BOARD_DATA;

/*--------------------------------------------------------------------------*/
/* public functions                                                         */
/*--------------------------------------------------------------------------*/

void board_init(BOARD_DATA *board);
int board_find_actor(BOARD_DATA *board, int type, int *ax, int *ay);
void board_start_game(BOARD_DATA *board, int game, int light_first);
int board_end_game(BOARD_DATA *board);
int board_frame(BOARD_DATA *board);

int board_pause_game(BOARD_DATA *board, int pause);
void board_refresh(BOARD_DATA *board);

/* return the dir (or fire command) needed to reach the specified move. */
/* side effect: resets dir_held and fire_down in board->iface */
/* returns 0 if the move is complete OR if the move is invalid */
int board_next_step(BOARD_DATA *board, COMMAND cmd);

/* return true if hitting key will complete a legal move */
int board_move_complete(BOARD_DATA *board, COMMAND cmd, int key);

void board_absolute_control_delta(BOARD_DATA *board, int *dx, int *dy);

/* wrappers for board_rules functions */

int board_side(BOARD_DATA *board);
int board_elem_num(BOARD_DATA *board);

/*--------------------------------------------------------------------------*/
/* private functions                                                        */
/*--------------------------------------------------------------------------*/

void board_init_field(BOARD_DATA *board, int xd, int yd);
int board_field(BOARD_DATA *board);
int board_input_ready(BOARD_DATA *board);

void board_update_state(BOARD_DATA *board);
void board_init_spell(BOARD_DATA *board);
void board_init_spell_source(BOARD_DATA *board);
void board_init_spell_target(BOARD_DATA *board);

void board_set_cmove(BOARD_DATA *board);

void board_make_move(BOARD_DATA *board, int x, int y);
void board_field_done(BOARD_DATA *board, int attacker_result);

void board_start_turn(BOARD_DATA *board, int swap_side);

int board_step(BOARD_DATA *board, int dir);
int board_verify_step(BOARD_DATA *board, int dir);
int board_verify_cursor_step(BOARD_DATA * board, int dir);
int board_verify_menu_step(BOARD_DATA * board, int dir);
int board_verify_spell_source_step(BOARD_DATA * board, int dir);
int board_verify_spell_target_step(BOARD_DATA * board, int dir);
int board_verify_revive_source_step(BOARD_DATA * board, int dir);
int board_verify_revive_target_step(BOARD_DATA *board, int dir);
void board_update_cursor(BOARD_DATA *board, int dir);

int board_selection(BOARD_DATA *board);
int board_verify_selection(BOARD_DATA *board);
int board_commit_selection(BOARD_DATA *board);
int board_commit_source(BOARD_DATA *board, int x, int y);
int board_commit_target(BOARD_DATA *board, int x, int y);
int board_commit_spell(BOARD_DATA *board, int y);
int board_commit_spell_source(BOARD_DATA *board, int x, int y);
int board_commit_spell_target(BOARD_DATA *board, int x, int y);

int board_command_matches(BOARD_DATA *board, COMMAND cmd);
int board_ctarget_cell_x(BOARD_DATA * board, COMMAND cmd);
int board_ctarget_cell_y(BOARD_DATA * board, COMMAND cmd);
int board_spell_cell_y(BOARD_DATA * board, int spell);

int board_animate_board(BOARD_DATA *board);
void board_cursor(BOARD_DATA *board);
void board_paint_cell(BOARD_DATA *board, int x, int y);
void board_paint_revive_options(BOARD_DATA *board);
void board_paint_revive_option(BOARD_DATA *board, int i);
void board_paint_cursor(BOARD_DATA *board);

void board_cursor_message(BOARD_DATA *board, int y);
void board_spell_message(BOARD_DATA *board, int spell);
void board_start_game_message(BOARD_DATA *board);
void board_message(BOARD_DATA *board, int message_code);
void board_ssp_init(BOARD_DATA *board, 
		    int x0, int y0, int x1, int y1, int both);
int board_cursor_overlaps(BOARD_DATA *board, int x, int y);

int board_cursor_cell_x(BOARD_DATA *board);
int board_cursor_cell_y(BOARD_DATA *board);
int board_cursor_cell_xsize(BOARD_DATA *board);
int board_cursor_cell_ysize(BOARD_DATA *board);
int board_cursor_inc(BOARD_DATA *board);
int board_cursor_pix_x(BOARD_DATA *board);
int board_cursor_pix_y(BOARD_DATA *board);
int board_cursor_min_x(BOARD_DATA *board);
int board_cursor_min_y(BOARD_DATA *board);
int board_cursor_max_x(BOARD_DATA *board);
int board_cursor_max_y(BOARD_DATA *board);
int board_cursor_min_cell_x(BOARD_DATA *board);
int board_cursor_min_cell_y(BOARD_DATA *board);
int board_cursor_max_cell_x(BOARD_DATA *board);
int board_cursor_max_cell_y(BOARD_DATA *board);
int board_cursor_appearance(BOARD_DATA *board);
ACTOR * board_selected_actor(BOARD_DATA *board);

void board_iface_init(BOARD_IFACE *iface);
void board_iface_reset(BOARD_IFACE *iface);
int board_iface_select(BOARD_IFACE *iface);
int board_iface_dir(BOARD_IFACE *iface, int mode);
int board_iface_menudir_held(BOARD_IFACE *iface);

#endif /* __MY_BOARD_H */
