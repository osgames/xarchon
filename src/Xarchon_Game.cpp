#include "Xarchon_Game.hpp"

Xarchon_Game::Xarchon_Game(void) : seq(3,10,3,10)
{
  state=NULL;
}

Xarchon_Game::~Xarchon_Game(void)
{
  if (state!=NULL)
    delete state;
}

void Xarchon_Game::SetPlayer(Xarchon_Goal_Test *light,Xarchon_Goal_Test *dark)
{
  for (int i=0;i<TOTAL_VALUE_PARMS;i++) {
    ((Xarchon_Goal_Test *)seq.light->gt)->value_parm[i]=light->value_parm[i];
    ((Xarchon_Goal_Test *)seq.dark->gt )->value_parm[i]=dark->value_parm[i];
  }
}

void Xarchon_Game::InitStart(int light_first)
{
  if (state!=NULL)
    delete state;
  state=new Xarchon_State;
  state->Start_Game(0, light_first);
}

void Xarchon_Game::Display(void)
{
  for (int y = 0; y < state->Height(); y++) {
    printf ("\n");
    for (int x = 0; x < state->Width(); x++) {
      if (!state->Empty(x,y)) {
	if (state->Actor_Side(x,y) == 0) 
	  printf("%c",*state->Actor(x,y)->name); // light
	else
	  printf("%c",*state->Actor(x,y)->name-32); // dark
      }
      else 
	printf (" ");
    }
  }
  printf ("\nTurn Number %d \n",state->Turn());
}

void Xarchon_Game::Play(void)
{
  while (!CheckEnd()) {
    Display();
    Move();
  }
}

void Xarchon_Game::Move(void)
{
  Xarchon_Strategy *strategy=(Xarchon_Strategy *)seq.Next_Strategy(state);
  strategy->InitState(state);
  Operator<Xarchon_State> *oper=strategy->Next();
  Xarchon_State *next_state=oper->Operate(state);
  delete oper;
  delete state;
  state=next_state;
  if (state->Fight())
    Move();
}


bool Xarchon_Game::CheckEnd(void)
{
  if (state->Fight())
    return false;
  
  if (state->EndOfGame())
    return true;

  if (state->Turn()>=10)
    return true;
  return false;
}
