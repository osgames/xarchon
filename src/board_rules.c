/*--------------------------------------------------------------------------*/
/* game board rules                                                         */
/*--------------------------------------------------------------------------*/

#include <config.h>
/* uncomment any includes that are actually needed

#include <stdarg.h>
*/
#include <stdio.h>
#include <stdlib.h>
/*
#include <string.h>
*/
#include "main.h"
/*
#include "field.h"
*/
#include "sprite.h"
/*#include "canvas.h"
#include "iface.h"
#include "audio.h"
*/

#include "actors.h"
#include "board_const.h"
#include "board_rules.h"

/*--------------------------------------------------------------------------*/
/* defines                                                                  */
/*--------------------------------------------------------------------------*/

int init_board_cells[NUM_GAMES][BOARD_YCELLS][BOARD_XCELLS] = {
   /* the board for Archon */
   {
      { ( CELL_DARK  | ACTOR_VALKYRIE            ), ( CELL_LIGHT | ACTOR_ARCHER ), ( CELL_DARK  ), ( CELL_LUMI  ), ( CELL_LUMI | CELL_POWER ), ( CELL_LUMI  ), ( CELL_LIGHT ), ( CELL_DARK  | ACTOR_MANTICORE ), ( CELL_LIGHT | ACTOR_BANSHEE                ) },
      { ( CELL_LIGHT | ACTOR_GOLEM               ), ( CELL_DARK  | ACTOR_KNIGHT ), ( CELL_LUMI  ), ( CELL_LIGHT ), ( CELL_LUMI              ), ( CELL_DARK  ), ( CELL_LUMI  ), ( CELL_LIGHT | ACTOR_GOBLIN    ), ( CELL_DARK  | ACTOR_TROLL                  ) },
      { ( CELL_DARK  | ACTOR_UNICORN             ), ( CELL_LUMI  | ACTOR_KNIGHT ), ( CELL_LIGHT ), ( CELL_DARK  ), ( CELL_LUMI              ), ( CELL_LIGHT ), ( CELL_DARK  ), ( CELL_LUMI  | ACTOR_GOBLIN    ), ( CELL_LIGHT | ACTOR_BASILISK               ) },
      { ( CELL_LUMI  | ACTOR_DJINNI              ), ( CELL_LIGHT | ACTOR_KNIGHT ), ( CELL_DARK  ), ( CELL_LIGHT ), ( CELL_LUMI              ), ( CELL_DARK  ), ( CELL_LIGHT ), ( CELL_DARK  | ACTOR_GOBLIN    ), ( CELL_LUMI  | ACTOR_SHAPESHIFTER           ) },
      { ( CELL_LIGHT | ACTOR_WIZARD | CELL_POWER ), ( CELL_LUMI  | ACTOR_KNIGHT ), ( CELL_LUMI  ), ( CELL_LUMI  ), ( CELL_LUMI | CELL_POWER ), ( CELL_LUMI  ), ( CELL_LUMI  ), ( CELL_LUMI  | ACTOR_GOBLIN    ), ( CELL_DARK  | ACTOR_SORCERESS | CELL_POWER ) },
      { ( CELL_LUMI  | ACTOR_PHOENIX             ), ( CELL_LIGHT | ACTOR_KNIGHT ), ( CELL_DARK  ), ( CELL_LIGHT ), ( CELL_LUMI              ), ( CELL_DARK  ), ( CELL_LIGHT ), ( CELL_DARK  | ACTOR_GOBLIN    ), ( CELL_LUMI  | ACTOR_DRAGON                 ) },
      { ( CELL_DARK  | ACTOR_UNICORN             ), ( CELL_LUMI  | ACTOR_KNIGHT ), ( CELL_LIGHT ), ( CELL_DARK  ), ( CELL_LUMI              ), ( CELL_LIGHT ), ( CELL_DARK  ), ( CELL_LUMI  | ACTOR_GOBLIN    ), ( CELL_LIGHT | ACTOR_BASILISK               ) },
      { ( CELL_LIGHT | ACTOR_GOLEM               ), ( CELL_DARK  | ACTOR_KNIGHT ), ( CELL_LUMI  ), ( CELL_LIGHT ), ( CELL_LUMI              ), ( CELL_DARK  ), ( CELL_LUMI  ), ( CELL_LIGHT | ACTOR_GOBLIN    ), ( CELL_DARK  | ACTOR_TROLL                  ) },
      { ( CELL_DARK  | ACTOR_VALKYRIE            ), ( CELL_LIGHT | ACTOR_ARCHER ), ( CELL_DARK  ), ( CELL_LUMI  ), ( CELL_LUMI | CELL_POWER ), ( CELL_LUMI  ), ( CELL_LIGHT ), ( CELL_DARK  | ACTOR_MANTICORE ), ( CELL_LIGHT | ACTOR_BANSHEE                ) }
   },
   /* the board for Adept */
   {
      { (CELL_FIRE | CELL_POWER), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE | CELL_POWER) },
      { (CELL_FIRE), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_VOID | CELL_POWER), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_FIRE) },
      { (CELL_FIRE), (CELL_AIR), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_AIR), (CELL_FIRE) },
      { (CELL_FIRE), (CELL_AIR), (CELL_WATER), (CELL_EARTH), (CELL_EARTH), (CELL_EARTH), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_EARTH), (CELL_EARTH), (CELL_EARTH), (CELL_WATER), (CELL_AIR), (CELL_FIRE) },
      { (CELL_FIRE /*| ACTOR_CHAOS_ADEPT*/), (CELL_AIR /*| ACTOR_CHAOS_ADEPT */), (CELL_WATER /*| ACTOR_CHAOS_ADEPT */), (CELL_EARTH /*| ACTOR_CHAOS_ADEPT */), (CELL_CHAOS_CITADEL), (CELL_EARTH), (CELL_EARTH), (CELL_EARTH), (CELL_EARTH), (CELL_EARTH), (CELL_EARTH), (CELL_EARTH), (CELL_ORDER_CITADEL), (CELL_EARTH /*| ACTOR_ORDER_ADEPT */), (CELL_WATER /*| ACTOR_ORDER_ADEPT */), (CELL_AIR /*| ACTOR_ORDER_ADEPT */), (CELL_FIRE /*| ACTOR_ORDER_ADEPT */) },
      { (CELL_FIRE), (CELL_AIR), (CELL_WATER), (CELL_EARTH), (CELL_EARTH), (CELL_EARTH), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_EARTH), (CELL_EARTH), (CELL_EARTH), (CELL_WATER), (CELL_AIR), (CELL_FIRE) },
      { (CELL_FIRE), (CELL_AIR), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_WATER), (CELL_AIR), (CELL_FIRE) },
      { (CELL_FIRE), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_VOID | CELL_POWER), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_AIR), (CELL_FIRE) },
      { (CELL_FIRE | CELL_POWER), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE), (CELL_FIRE | CELL_POWER) }
   }
};

int init_spell_avails[SPELL_COUNT_2] = { 0, 1, 1, 1, 1, 1, 1, 1, 1, 0 };

/*--------------------------------------------------------------------------*/
/* public functions                                                         */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* board_rules_init                                                         */
/*--------------------------------------------------------------------------*/

void board_rules_init(BOARD_STATE *state)
{
   /* setup initial board[] data */
   memset(state, 0, sizeof(BOARD_STATE));
}

/*--------------------------------------------------------------------------*/
/* board_rules_copy                                                         */
/*--------------------------------------------------------------------------*/

void board_rules_copy(BOARD_STATE *state, BOARD_STATE *orig)
{
   int x, y;

   state->side = board_rules_side(orig);
   state->turn = board_rules_turn(orig);
   state->lumi = board_rules_lumi(orig);
   state->lumi_d = board_rules_lumi_d(orig);

   state->winner = orig->winner;
   state->cell_w = orig->cell_w;
   state->cell_h = orig->cell_h;
   state->game = orig->game;

   memcpy (state->spell_avails, orig->spell_avails, 
             sizeof(orig->spell_avails));

   cell_copy(&state->elem, &orig->elem);

   for (y = 0; y < state->cell_h; y++)
      for (x = 0; x < state->cell_w; x++)
         cell_copy(board_rules_cell(state, x, y), 
                   board_rules_cell(orig, x, y));
}

/*--------------------------------------------------------------------------*/
/* board_rules_clear                                                        */
/*--------------------------------------------------------------------------*/

void board_rules_clear(BOARD_STATE *state)
{
   int x, y;

   /* this was in board_end_game. */
   for (y = 0; y < state->cell_h; y++)
      for (x = 0; x < state->cell_w; x++)
         if (!board_rules_empty(state, x, y)) {
            board_rules_cell_clear(state, x, y);
         }
   if (board_rules_elem_actor(state) != NULL)
      cell_clear(&state->elem);
}

/*--------------------------------------------------------------------------*/
/* board_rules_side                                                         */
/*--------------------------------------------------------------------------*/

int board_rules_side(BOARD_STATE *state)
{
   return state->side;
}

/*--------------------------------------------------------------------------*/
/* board_rules_turn                                                         */
/*--------------------------------------------------------------------------*/

int board_rules_turn(BOARD_STATE *state)
{
   return state->turn;
}

/*--------------------------------------------------------------------------*/
/* board_rules_lumi                                                         */
/*--------------------------------------------------------------------------*/

int board_rules_lumi(BOARD_STATE *state)
{
   return state->lumi;
}

/*--------------------------------------------------------------------------*/
/* board_rules_lumi_d                                                       */
/*--------------------------------------------------------------------------*/

int board_rules_lumi_d(BOARD_STATE *state)
{
   return state->lumi_d;
}

/*--------------------------------------------------------------------------*/
/* board_rules_cell                                                         */
/*--------------------------------------------------------------------------*/

CELL * board_rules_cell(BOARD_STATE *state, int x, int y)
{
   return &state->cells[y][x];
}

/*--------------------------------------------------------------------------*/
/* board_rules_field_cell                                                   */
/*--------------------------------------------------------------------------*/

CELL * board_rules_field_cell(BOARD_STATE *state, COMMAND cmd)
{
   return board_rules_cell(state, cmd.b.x1, cmd.b.y1);
}

/*--------------------------------------------------------------------------*/
/* board_rules_cell_lumi                                                    */
/*--------------------------------------------------------------------------*/

int board_rules_cell_lumi(BOARD_STATE *state, int x, int y)
{
   CELL * cell;
   cell = board_rules_cell(state, x, y);

   return cell_lumi(cell, state->lumi);
}

/*--------------------------------------------------------------------------*/
/* board_rules_cell_init                                                    */
/*--------------------------------------------------------------------------*/

void board_rules_cell_init(BOARD_STATE *state, int x, int y, int actor_num)
{
   CELL *cell;
   cell = board_rules_cell(state, x, y);
   cell_init(cell, actor_num);
}

/*--------------------------------------------------------------------------*/
/* board_rules_cell_clear                                                   */
/*--------------------------------------------------------------------------*/

void board_rules_cell_clear(BOARD_STATE *state, int x, int y)
{
   CELL *cell;
   cell = board_rules_cell(state, x, y);
   if (cell->actor == board_rules_elem_actor(state))
      state->elem.actor = NULL;
   cell_clear(cell);
}

/*--------------------------------------------------------------------------*/
/* board_rules_actor                                                        */
/*--------------------------------------------------------------------------*/

ACTOR * board_rules_actor(BOARD_STATE *state, int x, int y)
{
   return state->cells[y][x].actor;
}

/*--------------------------------------------------------------------------*/
/* board_rules_elem_actor                                                   */
/*--------------------------------------------------------------------------*/

ACTOR * board_rules_elem_actor(BOARD_STATE *state)
{
   return state->elem.actor;
}

/*--------------------------------------------------------------------------*/
/* board_rules_attacker                                                     */
/*--------------------------------------------------------------------------*/

ACTOR * board_rules_attacker(BOARD_STATE *state, COMMAND cmd)
{
   if (cmd.b.spell == 0)
      return board_rules_actor(state, cmd.b.ax0, cmd.b.ay0);
   else {
      if (cmd.b.spell == SPELL_SUMMON_ELEMENTAL) {
         /* make sure an elemental has been set */
         board_rules_elem_num(state, main_random() % 4);
         return board_rules_elem_actor(state);
      }
      else
         return board_rules_actor(state, cmd.b.sx0, cmd.b.sy0);
   }
}

/*--------------------------------------------------------------------------*/
/* board_rules_defender                                                     */
/*--------------------------------------------------------------------------*/

ACTOR * board_rules_defender(BOARD_STATE *state, COMMAND cmd)
{
   return board_rules_actor(state, cmd.b.x1, cmd.b.y1);
}

/*--------------------------------------------------------------------------*/
/* board_rules_actor_side                                                   */
/*--------------------------------------------------------------------------*/

int board_rules_actor_side(BOARD_STATE *state, int x, int y)
{
   return actor_side(board_rules_actor(state, x, y));
}

/*--------------------------------------------------------------------------*/
/* board_rules_actor_range                                                  */
/*--------------------------------------------------------------------------*/

int board_rules_actor_range(BOARD_STATE *state, int x, int y)
{
   return actor_range(board_rules_actor(state, x, y));
}

/*--------------------------------------------------------------------------*/
/* board_rules_actor_is_master                                              */
/*--------------------------------------------------------------------------*/

int board_rules_actor_is_master(BOARD_STATE *state, int x, int y)
{
   return (actor_is_master(board_rules_actor(state, x, y)));
}

/*--------------------------------------------------------------------------*/
/* board_rules_find_actor                                                   */
/*--------------------------------------------------------------------------*/

int board_rules_find_actor(BOARD_STATE *state, int type, int *ax, int *ay)
{
   int x, y;

   for (y = 0; y < state->cell_h; y++)
      for (x = 0; x < state->cell_w; x++)
         if (state->cells[y][x].actor != NULL &&
             (state->cells[y][x].actor->type & ACTOR_MASK) == type) {
            *ax = x;
            *ay = y;
            return 1;
         }
   *ax = -1;
   *ay = -1;
   return 0;
}

/*--------------------------------------------------------------------------*/
/* board_rules_elem_num                                                     */
/*--------------------------------------------------------------------------*/

/* return the number of the current elemental:                              */
/* 0-3 = air, earth, fire, water                                            */
/* if there is no elemental set, use the elem_num parameter to choose it    */

/* the elem_num argument should be a number 0-3.                            */
/* (normally random, of course, but the AI will want to                     */
/* try all 4 settings)                                                      */

/* the return value should be assigned to sy0 (spell source y)              */
/* by the thing making a move.                                              */
/* (though I'm not sure it makes a difference as long as it gets called)    */

/* do we still need a board_rules_set_elem_actor? */
/* maybe just a board_rules_clear_elem. */

int board_rules_elem_num(BOARD_STATE *state, int elem_num)
{
   if (board_rules_elem_actor(state) == NULL) {
      state->elem.flags = elem_num;
      cell_init(&state->elem, ACTOR_ELEM_FIRST + state->elem.flags);
      board_rules_elem_actor(state)->type |= (!state->side) * ACTOR_LIGHT;
   }
   else
      elem_num = board_rules_elem_actor(state)->type - ACTOR_AIR_ELEM;

   return elem_num;
}

/*--------------------------------------------------------------------------*/
/* board_rules_power_point                                                  */
/*--------------------------------------------------------------------------*/

int board_rules_power_point(BOARD_STATE *state, int x, int y)
{
   return (state->cells[y][x].flags & CELL_POWER) == CELL_POWER;
}

/*--------------------------------------------------------------------------*/
/* board_rules_cell_animated                                                */
/*--------------------------------------------------------------------------*/

int board_rules_cell_animated(BOARD_STATE *state, int x, int y)
{
   return state->cells[y][x].flags & CELL_ANIM_MASK;
}

/*--------------------------------------------------------------------------*/
/* board_rules_empty                                                        */
/*--------------------------------------------------------------------------*/

int board_rules_empty(BOARD_STATE *state, int x, int y)
{
   return state->cells[y][x].actor == NULL;
}

/*--------------------------------------------------------------------------*/
/* board_rules_width                                                        */
/*--------------------------------------------------------------------------*/

int board_rules_width(BOARD_STATE *state)
{
   return state->cell_w;
}

/*--------------------------------------------------------------------------*/
/* board_rules_height                                                       */
/*--------------------------------------------------------------------------*/

int board_rules_height(BOARD_STATE *state)
{
   return state->cell_h;
}

/*--------------------------------------------------------------------------*/
/* board_rules_in_range                                                     */
/*--------------------------------------------------------------------------*/

int board_rules_in_range(BOARD_STATE *state, int x, int y)
{
   return  (y >= 0 && y < state->cell_h && x >= 0 && x < state->cell_w);
}

/*--------------------------------------------------------------------------*/
/* board_rules_imprisoned                                                   */
/*--------------------------------------------------------------------------*/

int board_rules_imprisoned(BOARD_STATE *state, int x, int y)
{
   return state->cells[y][x].flags & CELL_IMPRISON;
}

/*--------------------------------------------------------------------------*/
/* board_rules_start_game                                                   */
/*--------------------------------------------------------------------------*/

void board_rules_start_game(BOARD_STATE *state, int game, int light_first)
{
   int x, y;
   CELL * cell;

   state->winner = WINNER_NOT_YET;
   state->game = game;
   state->side = !light_first;

   switch (game) {
   case GAME_ARCHON:
      state->cell_w = 9;
      state->cell_h = 9;
      break;
   case GAME_ADEPT:
      state->cell_w = 17;
      state->cell_h = 9;
      break;
   }
   state->lumi   = light_first ? LUMI_LIGHTEST : LUMI_DARKEST;
   state->lumi_d = light_first ? -1            : 1;
   /* setup actors on the board */
   for (y = 0; y < state->cell_h; y++)
      for (x = 0; x < state->cell_w; x++) {
         cell = &state->cells[y][x];
         cell->flags = init_board_cells[game][y][x];
         if ((cell->flags & ACTOR_MASK) != 0)
            cell_init(cell, cell->flags & ACTOR_MASK);
         else
            cell->actor = NULL;
      }
   for (y = 0; y <= 1; y++)
      memcpy(state->spell_avails[y], init_spell_avails, 
             sizeof(init_spell_avails));

   state->elem.actor = NULL;
   state->routes.ax0 = -1;
}

/*--------------------------------------------------------------------------*/
/* board_rules_make_move                                                    */
/*--------------------------------------------------------------------------*/

/* assumes the move has already been checked by the verify functions,
 * is legal, and requires no field battle 
 */

int board_rules_make_move(BOARD_STATE *state, COMMAND cmd)
{
   state->spell_avails[state->side][cmd.b.spell] = 0;

   switch (cmd.b.spell) {
   case 0:
      board_rules_move_actor(state, cmd.b.ax0, cmd.b.ay0, cmd.b.x1, cmd.b.y1);
      break;
   case SPELL_TELEPORT:
      board_rules_move_actor(state, cmd.b.sx0, cmd.b.sy0, cmd.b.x1, cmd.b.y1);
      break;
   case SPELL_HEAL:
      board_rules_cast_heal(state, cmd.b.sx0, cmd.b.sy0);
      break;
   case SPELL_SHIFT_TIME:
      board_rules_cast_shift_time(state);
      break;
   case SPELL_EXCHANGE:
      board_rules_cast_exchange(state, cmd.b.sx0, cmd.b.sy0, cmd.b.x1, cmd.b.y1);
      break;
   case SPELL_REVIVE:
      board_rules_cast_revive(state, cmd.b.sy0, cmd.b.x1, cmd.b.y1);
      break;
   case SPELL_IMPRISON:
      board_rules_cast_imprison(state, cmd.b.sx0, cmd.b.sy0);
      break;
   default:
      /* we should never get here */
      /* (SUMMON_ELEMENTAL always results in a field battle) */
   }
   return board_rules_end_turn(state);
}

/*--------------------------------------------------------------------------*/
/* board_rules_field_winner                                                 */
/*--------------------------------------------------------------------------*/

int board_rules_field_winner(BOARD_STATE *state, COMMAND cmd, int field_result)
{
   CELL *defender, *attacker;

   state->spell_avails[state->side][cmd.b.spell] = 0;
   /* this is duplication. but then, this function and make_move are
      already sort of duplication. */

   defender = board_rules_cell(state, cmd.b.x1, cmd.b.y1);

   if (cmd.b.spell == 0)
      attacker = board_rules_cell(state, cmd.b.ax0, cmd.b.ay0);
   else if (cmd.b.spell == SPELL_SUMMON_ELEMENTAL)
      attacker = NULL;
   else if (cmd.b.spell == SPELL_TELEPORT)
      attacker = board_rules_cell(state, cmd.b.sx0, cmd.b.sy0);
   else {
      attacker = NULL;
      /* we should NEVER get here! */
   }
  
   if (field_result == BOARD_FIELD_ATTACKER_WIN) {
      cell_clear(defender);
      if (attacker) {
         /* check for elemental? */
         actor_reset(attacker->actor);
         defender->actor = attacker->actor;
      }
   } else if (field_result == BOARD_FIELD_DEFENDER_WIN) {
      actor_reset(defender->actor);
      if (attacker)
         cell_clear(attacker);
   } else {
      cell_clear(defender);
      if (attacker)
         cell_clear(attacker);
   }
   if (attacker)
      attacker->actor = NULL;
  
   /* is this the proper way to dispose of the elemental? */
   if (state->elem.actor != NULL) {
      cell_clear(&state->elem);
   }

   return board_rules_end_turn(state);
}

/*--------------------------------------------------------------------------*/
/* board_rules_winner                                                       */
/*--------------------------------------------------------------------------*/

int board_rules_winner(BOARD_STATE *state)
{
   return state->winner;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_move                                                  */
/*--------------------------------------------------------------------------*/

int board_rules_verify_move(BOARD_STATE *state, COMMAND cmd)
{
  int result;
  int spell = cmd.b.spell;
  int ax0 = cmd.b.ax0;
  int ay0 = cmd.b.ay0;
  int sx0 = cmd.b.sx0;
  int sy0 = cmd.b.sy0;
  int x1 = cmd.b.x1;
  int y1 = cmd.b.y1;
  
  result = board_rules_verify_source(state, ax0, ay0);
  if (result != BOARD_NEED_TARGET)
    return result;

  if (spell) {
    result = board_rules_verify_spell(state, ax0, ay0, spell);
    if (result == BOARD_NEED_SPELL_SOURCE)
      result = board_rules_verify_spell_source(state, spell, sx0, sy0);
    if (result == BOARD_NEED_SPELL_TARGET)
      result = board_rules_verify_spell_target(state, spell, ax0, ay0,
                                               sx0, sy0, x1, y1);
  }
  else {
    result = board_rules_verify_actor_target(state, ax0, ay0, x1, y1);
  }
  return result;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_source                                                */
/*--------------------------------------------------------------------------*/

int board_rules_verify_source(BOARD_STATE *state, int x0, int y0)
{
  
   int distance;
   int x,y;
   ACTOR * actor;

   if (board_rules_empty(state, x0, y0))
      return BOARD_NOT_YOURS;

   actor = board_rules_actor(state, x0, y0);

   if (!actor_is_side(actor, state->side))
      return BOARD_NOT_YOURS;

   if (board_rules_imprisoned(state, x0, y0))
      return BOARD_IMPRISONED;

   if (actor_is_master(actor))
      return BOARD_NEED_TARGET;

   distance = (actor_is_ground(actor) ? 1 : actor_range(actor));

   for (y = y0 - distance; y <= y0 + distance; y++)
      if (y >= 0 && y <= state->cell_h - 1)
         for (x = x0 - distance; x <= x0 + distance; x++) {

            if (x < 0 || x > state->cell_w - 1)
               continue;

            if (actor_is_ground(actor) && vabs(x - x0) == 1 && vabs(y - y0) == 1)
               continue;

            if (board_rules_empty(state, x, y) || 
                !actor_is_side(board_rules_actor(state, x, y), state->side))
               return BOARD_NEED_TARGET;
         }
   return BOARD_CANT_MOVE;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_actor_step                                            */
/*--------------------------------------------------------------------------*/

int board_rules_verify_actor_step(BOARD_STATE *state, int x0, int y0, 
				  int x1, int y1, int dir)
{
   ACTOR * actor;

   int x2 = x1 + state_move_x_step[dir];
   int y2 = y1 + state_move_y_step[dir];

   if (!board_rules_in_range(state, x2, y2))
      return BOARD_OUT_OF_RANGE;
   
   actor = board_rules_actor(state, x0, y0);

   /* ground creatures 'must attack or retreat' */
   /* (can we do this without accessing the sprite?) */
   if (actor_is_ground(actor) && !board_rules_empty(state, x1, y1) &&
       actor_is_side(board_rules_actor(state, x1, y1), !state->side) &&
       dir != state_opposite[sprite_get_state(actor->sprite)])
      return BOARD_OPPOSED;

   if (x2 == x0 && y2 == y0)
      return BOARD_STEP_OK; /* back where we started */

   if (actor_is_ground(actor) && !board_rules_empty(state, x2, y2) &&
       actor_is_side(board_rules_actor(state, x2, y2), state->side))
      return BOARD_OCCUPIED;
   
   if (board_rules_route_exists(state, x0, y0, x2, y2))
      return BOARD_STEP_OK;

   return BOARD_MOVED_LIMIT;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_actor_target                                          */
/*--------------------------------------------------------------------------*/

int board_rules_verify_actor_target(BOARD_STATE *state, 
				    int x0, int y0, int x1, int y1)
{
   ACTOR * source;

   source = state->cells[y0][x0].actor;
   if (x1 == x0 && y1 == y0) {
      if ((source->type & ACTOR_MASTER) == ACTOR_MASTER)
         return BOARD_NEED_SPELL;
      else
         return BOARD_CANCEL;
   }

   if (!board_rules_route_exists(state, x0, y0, x1, y1))
      return BOARD_MOVED_LIMIT;

   return board_rules_verify_monster_target(state, x0, y0, x1, y1);
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_spell                                                 */
/*--------------------------------------------------------------------------*/

int board_rules_verify_spell(BOARD_STATE *state, int x, int y, int spell)
{
   if (!board_rules_actor_is_master(state, x, y))
      return BOARD_CANT_CAST_SPELLS;

   /* we should already have checked whether the caster is imprisoned,
      as part of board_rules_verify_source */

   if (spell < SPELL_FIRST || spell > SPELL_LAST)
      return BOARD_OUT_OF_RANGE;

   if (!state->spell_avails[state->side][spell])
      return BOARD_SPELL_UNAVAILABLE;

   switch (spell) {
   case SPELL_SHIFT_TIME:
      return BOARD_MOVE_OK;
   case SPELL_REVIVE:
      return board_rules_verify_revive(state, x, y);
   case SPELL_IMPRISON:
      return board_rules_verify_imprison(state);
   case SPELL_CEASE_CONJURING:
      return BOARD_CANCEL;
   default:
      return BOARD_NEED_SPELL_SOURCE;
   }
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_spell_source                                          */
/*--------------------------------------------------------------------------*/

int board_rules_verify_spell_source(BOARD_STATE *state, 
				    int spell, int x, int y)
{
   ACTOR *source;

   if (spell == SPELL_SUMMON_ELEMENTAL)
      return BOARD_NEED_SPELL_TARGET;

   if (spell == SPELL_REVIVE)
      return board_rules_verify_revive_source(state, y);

   if (board_rules_power_point(state, x, y) && spell != SPELL_REVIVE) 
      return BOARD_POWER_POINT;

   source = board_rules_actor(state, x, y);
   if (source == NULL) 
      return BOARD_EMPTY_SPELL_SOURCE;
  
   if (board_rules_imprisoned(state, x, y))
      return BOARD_IMPRISONED;

   switch (spell) {
   case SPELL_TELEPORT:
      if (!actor_is_side(source, state->side))
         return BOARD_NOT_YOURS;
      return BOARD_NEED_SPELL_TARGET;
   case SPELL_HEAL:
      if (!actor_is_side(source, state->side))
         return BOARD_NOT_YOURS;
      return BOARD_MOVE_OK;
   case SPELL_EXCHANGE:
      return BOARD_NEED_SPELL_TARGET;
   case SPELL_IMPRISON:
      if (actor_is_side(source, state->side))
         return BOARD_OCCUPIED;
      return BOARD_MOVE_OK;
   default:
      return 0;
      /* did we forget something? we should never get here */
   }
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_spell_target                                          */
/*--------------------------------------------------------------------------*/

int board_rules_verify_spell_target(BOARD_STATE *state, 
				    int spell, int x0, int y0, 
				    int sx0, int sy0, int x1, int y1)
{
   if (board_rules_power_point(state, x1, y1)) 
      return BOARD_POWER_POINT;

   switch (spell) {
   case SPELL_TELEPORT:
      return board_rules_verify_teleport_target(state, x0, y0, x1, y1);
   case SPELL_EXCHANGE:
      /* check for imprison? */
      return board_rules_verify_exchange_target(state, x1, y1);
   case SPELL_SUMMON_ELEMENTAL:
      /* should this be a source or a target? */
      return board_rules_verify_elemental_target(state, x0, y0, x1, y1);
   case SPELL_REVIVE:
      return board_rules_verify_revive_target(state, x0, y0, x1, y1);
   default:
      return 0;
      /* we should never get here */
   }
}

/*--------------------------------------------------------------------------*/
/* board_rules_in_revive_target_range                                       */
/*--------------------------------------------------------------------------*/

int board_rules_in_revive_target_range(BOARD_STATE *state, 
				       int master_x, int master_y, 
				       int x, int y)
{
   if (vabs(master_x - x) > 1 || vabs(master_y - y) > 1) {
#ifdef DEBUG_BOARD_RULES
      printf("board_rules_in_revive_target_range: %d,%d is too far from master at %d,%d\n", x,y,master_x,master_y);
#endif
      return 0;
   }

   if (!board_rules_in_range(state, x, y)) {
#ifdef DEBUG_BOARD_RULES
      printf("board_rules_in_revive_target_range: %d,%d is not on the board\n",
             x,y);
#endif
      return 0;
   }
#ifdef DEBUG_BOARD_RULES
   printf("board_rules_in_revive_target_range: %d,%d is in range of master %d,%d\n", x,y,master_x,master_y);
#endif
   return 1;
}
     
/*--------------------------------------------------------------------------*/
/* board_rules_route_exists                                                 */
/*--------------------------------------------------------------------------*/

/* returns true if the actor at x0, y0 can go to x1, y1 
 * even if x1, y1 is an illegal destination */

int board_rules_route_exists(BOARD_STATE *state, 
			     int ax, int ay, int x1, int y1)
{
   if (ax == x1 && ay == y1)
      return 1;
   return (board_rules_next_actor_step(state, ax, ay, ax, ay, x1, y1) != 0);
}

/*--------------------------------------------------------------------------*/
/* board_rules_next_actor_step                                              */
/*--------------------------------------------------------------------------*/

/* returns the next direction to go
 * for the actor at ax, ay to get from x0, y0 to x1, y1.
 * (even if x1, y1 is an illegal destination)
 * if there is no legal route found, returns 0. */

int board_rules_next_actor_step(BOARD_STATE *state, int ax, int ay, 
                               int x0, int y0, int x1, int y1)
{
   ACTOR * source = board_rules_actor(state, ax, ay);

   if (actor_is_ground(source)) {
      return board_rules_next_ground_step(state, ax, ay, x0, y0, x1, y1);
   }   
   else {
      if (!board_rules_in_fly_range(state, ax, ay, x1, y1)) {
         return 0;
      }
      return state_towards(x1, y1, x0, y0);
   }
}

/*--------------------------------------------------------------------------*/
/* board_rules_spell_count                                                  */
/*--------------------------------------------------------------------------*/

/* return the number of available spells (inc. CEASE_CONJURING)             */
/* choice = menu index, starting with 0                                     */
/* return value is the spell number for the current side                    */
/* return 0 if choice was out of range.                                     */

int board_rules_spell_count(BOARD_STATE *state)
{
   int spell, count;
  
   count = 0;
   for (spell = SPELL_FIRST; spell <= SPELL_LAST; spell++)
      if (state->spell_avails[state->side][spell])
         count++;

   return count;
}

/*--------------------------------------------------------------------------*/
/* board_rules_spell_choice                                                 */
/*--------------------------------------------------------------------------*/

/* convert a menu item number into a spell number.                          */
/* choice = menu index, starting with 0                                     */
/* return value is the spell number for the current side                    */
/* return 0 if choice was out of range.                                     */

int board_rules_spell_choice(BOARD_STATE *state, int choice)
{
   int next_choice, spell;
  
   /* calling spell_choice repeatedly with increasing values of choice 
    * will result in extra looping. If this is a problem, some caching
    * should be done. */

   next_choice = 0;
   for (spell = SPELL_FIRST; spell <= SPELL_LAST; spell++) {
      if (state->spell_avails[state->side][spell]) {
         if (next_choice == choice)
            return spell;
         else
            next_choice++;
      }
   }
   return 0;
}

/*--------------------------------------------------------------------------*/
/* board_rules_spell_index                                                  */
/*--------------------------------------------------------------------------*/

/* convert a spell number into a menu item number.                          */
/* spell should always be an available one.                                 */
/* (perhaps this function should crash if it is not?)                       */
/* return value = menu index, starting with 0                               */

/* in order to insure that spell_index is the inverse of spell_choice,      */
/* this function calls spell_choice. However this results in an additional  */
/* level of looping. Just so you know.                                      */

int board_rules_spell_index(BOARD_STATE *state, int spell)
{
   int i;
  
   for (i = 0; i < board_rules_spell_count(state); i++)
      if (board_rules_spell_choice(state, i) == spell)
	 return i;

   return board_rules_spell_count(state) - 1; /* should be CEASE_CONJURING. */
   /* but maybe we should just crash */
}

/*--------------------------------------------------------------------------*/
/* board_rules_revive_count                                                 */
/*--------------------------------------------------------------------------*/

/* return the number of different monsters that can be revived              */
/* by the current side                                                      */

int board_rules_revive_count(BOARD_STATE *state)
{
   int num_actors;
   int first, last, i;

   num_actors = 0;
   first = (state->side == 0) ? ACTOR_LIGHT_FIRST : ACTOR_DARK_FIRST;
   last = (state->side == 0) ? ACTOR_LIGHT_LAST : ACTOR_DARK_LAST;

   for (i = first; i <= last; i++)
      if (board_rules_can_revive_actor(state, i))
         num_actors++;
   return num_actors;
}

/*--------------------------------------------------------------------------*/
/* board_rules_revive_choice                                                */
/*--------------------------------------------------------------------------*/

/* return the actor number of the nth dead actor on the current side        */
/* (n = count)                                                              */

int board_rules_revive_choice(BOARD_STATE *state, int count)
{
   int actor, first, last, next_count;

   first = (state->side == 0) ? ACTOR_LIGHT_FIRST : ACTOR_DARK_FIRST;
   last = (state->side == 0) ? ACTOR_LIGHT_LAST : ACTOR_DARK_LAST;

   next_count = 0;
   for (actor=first; actor<=last; actor++) {
      if (board_rules_can_revive_actor(state, actor)) {
         if (next_count == count)
            return actor;
         else
            next_count++;
      }
   }
   /* count was out of range */
   return 0;
}

/*--------------------------------------------------------------------------*/
/* board_rules_spell_available                                              */
/*--------------------------------------------------------------------------*/

int board_rules_spell_available(BOARD_STATE *state, int side, int spell)
{
   return state->spell_avails[side][spell];
}

/*--------------------------------------------------------------------------*/
/* private functions                                                        */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* board_rules_cast_heal                                                    */
/*--------------------------------------------------------------------------*/

void board_rules_cast_heal(BOARD_STATE *state, int x, int y)
{
   state->cells[y][x].actor->strength =
      actors_list[state->cells[y][x].actor->type & ACTOR_MASK].strength;
}

/*--------------------------------------------------------------------------*/
/* board_rules_cast_shift_time                                              */
/*--------------------------------------------------------------------------*/

void board_rules_cast_shift_time(BOARD_STATE *state)
{
   int x, y;
   ACTOR *actor;

   /* end_turn will still be called, advancing luminance if it is time. */
   /* Is that the correct behavior? */

   state->lumi = (state->lumi == LUMI_DARKEST) ? LUMI_LIGHTEST :
      (state->lumi == LUMI_LIGHTEST) ? LUMI_DARKEST :
      state->lumi;
   state->lumi_d = -state->lumi_d;

   for (y = 0; y < state->cell_h; y++)
      for (x = 0; x < state->cell_w; x++)
         if (state->cells[y][x].flags & CELL_IMPRISON) {
            actor = state->cells[y][x].actor;
            /* un-imprison light actor if luminance cycle is lightest; */
            /* un-imprison dark actor if luminance cycle is darkest.   */
            if ((state->lumi == LUMI_LIGHTEST && actor->type & ACTOR_LIGHT) ||
                (state->lumi == LUMI_DARKEST && !(actor->type & ACTOR_LIGHT)))
               state->cells[y][x].flags ^= CELL_IMPRISON;
         }
}

/*--------------------------------------------------------------------------*/
/* board_rules_cast_exchange                                                */
/*--------------------------------------------------------------------------*/

void board_rules_cast_exchange(BOARD_STATE *state, 
			       int x0, int y0, int x1, int y1)
{
   ACTOR *actor;
   actor = state->cells[y0][x0].actor;
   state->cells[y0][x0].actor = state->cells[y1][x1].actor;
   state->cells[y1][x1].actor = actor;
   actor_reset(state->cells[y0][x0].actor);
   actor_reset(state->cells[y1][x1].actor);
}

/*--------------------------------------------------------------------------*/
/* board_rules_cast_revive                                                  */
/*--------------------------------------------------------------------------*/

void board_rules_cast_revive(BOARD_STATE *state, int y0, int x1, int y1)
{
   int actor_num;
   actor_num = board_rules_revive_choice(state, y0);
   board_rules_cell_init(state, x1, y1, actor_num);
}

/*--------------------------------------------------------------------------*/
/* board_rules_cast_imprison                                                */
/*--------------------------------------------------------------------------*/

void board_rules_cast_imprison(BOARD_STATE *state, int x, int y)
{
   state->cells[y][x].flags |= CELL_IMPRISON;
}

/*--------------------------------------------------------------------------*/
/* board_rules_move_actor                                                   */
/*--------------------------------------------------------------------------*/

void board_rules_move_actor(BOARD_STATE *state, int x0, int y0, int x1, int y1)
{
   ACTOR * actor = board_rules_actor(state, x0, y0);

   state->cells[y1][x1].actor = actor;
   state->cells[y0][x0].actor = NULL;
   actor_reset(actor);
}

/*--------------------------------------------------------------------------*/
/* board_rules_end_turn                                                     */
/*--------------------------------------------------------------------------*/

int board_rules_end_turn(BOARD_STATE *state)
{
   int x, y;
   CELL * cell;
   ACTOR * actor;

   state->turn++;
   if (state->turn % 2 == 0) {
      state->lumi += state->lumi_d;
      if (state->lumi == LUMI_DARKEST || state->lumi == LUMI_LIGHTEST)
         state->lumi_d = -state->lumi_d;
      for (y = 0; y < state->cell_h; y++)
         for (x = 0; x < state->cell_w; x++)
            if (state->cells[y][x].actor != NULL) {
               cell = &state->cells[y][x];
               actor = cell->actor;
               /* un-imprison light actor when luminance cycle is lightest; */
               /* un-imprison dark actor when luminance cycle is darkest.   */
               if (cell->flags & CELL_IMPRISON &&
                   ((state->lumi == LUMI_LIGHTEST && actor->type & ACTOR_LIGHT) ||
                    (state->lumi == LUMI_DARKEST && !(actor->type & ACTOR_LIGHT))))
                  cell->flags ^= CELL_IMPRISON;
               /* heal actors, twice as fast if on a power point */
               actor->strength =
                  vmin(actors_list[actor->type & ACTOR_MASK].strength,
                      actor->strength + HEALTH_SCALE*2*
                      (((cell->flags & CELL_POWER) == CELL_POWER) ? 2 : 1));
            }
   }

   if (state->elem.actor != NULL)
      cell_clear(&state->elem);
   state->routes.ax0 = -1; /* reset the ground distances cache */

   state->side = !state->side;

   if (board_rules_check_win(state))
      return BOARD_GAME_OVER;
   else
      return BOARD_NEED_SOURCE;

   /* important that winner be */
   /*    checked only after un-imprison */
   /*    is done */
}

/*--------------------------------------------------------------------------*/
/* board_rules_check_win                                                    */
/*--------------------------------------------------------------------------*/

/* this function is to be called at the beginning of a new turn.            */
/* It checks conditions that will not happen until the next turn.           */
/* is it called at the right time? is the behavior correct?                 */

int board_rules_check_win(BOARD_STATE *state)
{
   int x, y;
   int is_dark;
   int num_light = 0, num_dark = 0;
   int num_pp = 0, num_light_pp = 0, num_dark_pp = 0;
   int result;

   for (y = 0; y < state->cell_h; y++)
      for (x = 0; x < state->cell_w; x++) {
         if (board_rules_power_point(state, x, y))
            num_pp++;
         if (!board_rules_empty(state, x, y)) {
            is_dark = board_rules_actor_side(state, x, y);
            /* pretend the next turn has begun */
            state->side = !state->side;
            /* if an actor is on the side about to play, */
            /* count it only if it is a legal source */
            result = board_rules_verify_source(state, x, y);
            if (result == BOARD_NOT_YOURS || result == BOARD_NEED_TARGET) {
               num_light += is_dark == 0;
               num_dark  += is_dark == 1;
            }
            /* stop pretending */
            state->side = !state->side;

            /* count control of power points */
            if (board_rules_power_point(state, x, y)) {
               num_light_pp += is_dark == 0;
               num_dark_pp  += is_dark == 1;
            }
         }
      }

   if (num_light == 0 || num_dark_pp == num_pp)
      state->winner = WINNER_DARK;                    /* light loses */
   if (num_dark == 0 || num_light_pp == num_pp)
      state->winner += 2;                      /* dark loses, or it's a tie */
   if (state->winner != 0) {
      return 1;
   }
   return 0;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_revive                                                */
/*--------------------------------------------------------------------------*/

int board_rules_verify_revive(BOARD_STATE *state, int master_x, int master_y)
{
   if (!board_rules_revive_count(state))
      return BOARD_SPELL_NO_SOURCES;

   if (!board_rules_revive_targets(state, master_x, master_y))
      return BOARD_SPELL_NO_TARGETS;
  
   return BOARD_NEED_SPELL_SOURCE;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_imprison                                              */
/*--------------------------------------------------------------------------*/

int board_rules_verify_imprison(BOARD_STATE *state)
{
   int next_lumi;

   /* don't allow if luminance on next turn is max of enemy's side */
   next_lumi = state->lumi + state->lumi_d * ((state->turn + 1) % 2 == 0);
   if ((state->side == 0 && next_lumi == LUMI_DARKEST) ||
       (state->side == 1 && next_lumi == LUMI_LIGHTEST))
      return BOARD_SPELL_NO_SOURCES;
   return BOARD_NEED_SPELL_SOURCE;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_revive_source                                         */
/*--------------------------------------------------------------------------*/

/* this function takes a menu index, not an actor number, as a parameter   */

int board_rules_verify_revive_source(BOARD_STATE *state, int revive_num)
{
   int count = board_rules_revive_count(state);
  
   if (revive_num < 0 || revive_num > count)
      return BOARD_OUT_OF_RANGE;

   if (revive_num == count)
      return BOARD_CANCEL;

   return BOARD_NEED_SPELL_TARGET;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_teleport_target                                       */
/*--------------------------------------------------------------------------*/

int board_rules_verify_teleport_target(BOARD_STATE * state, 
				       int x0, int y0, int x1, int y1)
{
   return board_rules_verify_monster_target(state, x0, y0, x1, y1);
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_elemental_target                                      */
/*--------------------------------------------------------------------------*/

int board_rules_verify_elemental_target(BOARD_STATE * state, 
					int x0, int y0, int x1, int y1)
{
   if (board_rules_empty(state, x1, y1))
      return BOARD_EMPTY_SPELL_TARGET;

   /* the elemental must already have a cell on the board */
   return board_rules_verify_monster_target(state, x0, y0, x1, y1);
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_exchange_target                                       */
/*--------------------------------------------------------------------------*/

int board_rules_verify_exchange_target(BOARD_STATE * state, int x1, int y1)
{
   if (board_rules_empty(state, x1, y1))
      return BOARD_EMPTY_SPELL_TARGET;

   return BOARD_MOVE_OK;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_monster_target                                        */
/*--------------------------------------------------------------------------*/

int board_rules_verify_monster_target(BOARD_STATE *state, 
				      int x0, int y0, int x1, int y1)
{
   ACTOR * monster;
   ACTOR * target;
  
   monster = state->cells[y0][x0].actor;
   target = state->cells[y1][x1].actor;
   if (target != NULL) {
      if ((target->type & ACTOR_LIGHT) == (monster->type & ACTOR_LIGHT))
         return BOARD_OCCUPIED;
      else
         return BOARD_NEED_FIELD;
   }

   return BOARD_MOVE_OK;
}

/*--------------------------------------------------------------------------*/
/* board_rules_verify_revive_target                                         */
/*--------------------------------------------------------------------------*/

/* a bit of duplication with legal_revive_target - get rid of one? */

int board_rules_verify_revive_target(BOARD_STATE *state, 
				     int master_x, int master_y, 
				     int x1, int y1)
{
   if (!board_rules_in_revive_target_range(state, master_x, master_y, x1, y1))
      return BOARD_OUT_OF_RANGE;
  
   if (!board_rules_empty(state, x1, y1))
      return BOARD_OCCUPIED;

   return BOARD_MOVE_OK;
}

/*--------------------------------------------------------------------------*/
/* board_rules_revive_targets                                               */
/*--------------------------------------------------------------------------*/

/* return the number of cells where a revived creature can be placed */

int board_rules_revive_targets(BOARD_STATE *state, int master_x, int master_y)
{
   int cell_count = 0;
   int x, y;

   for (y = master_y - 1; y <= master_y + 1; y++) 
      for (x = master_x - 1; x <= master_x +1; x++)
         if (board_rules_legal_revive_target(state, master_x, master_y, x, y)) 
            cell_count++;

   return cell_count;
}
     
/*--------------------------------------------------------------------------*/
/* board_rules_can_revive_actor                                             */
/*--------------------------------------------------------------------------*/

/* need to check if the actor is the right side?                            */
/* this is called repeatedly by board_rules_revive_count                    */
/* and board_rules_dead_actor.                                              */
/* if it's too slow, either this function or the functions that call it     */
/* should do some sort of caching.                                          */

int board_rules_can_revive_actor(BOARD_STATE *state, int actor_num)
{
   int init_count, real_count;
   ACTOR * actor;
   int y, x;

   init_count = real_count = 0;
   for (y = 0; y < state->cell_h; y++)
      for (x = 0; x < state->cell_w; x++) {

         actor = board_rules_actor(state, x, y);
         if (actor && ((actor->type & ACTOR_MASK) == actor_num))
            real_count++;

         if ((init_board_cells[state->game][y][x] & ACTOR_MASK) == actor_num)
            init_count++;
      }

   return real_count < init_count;
}

/*--------------------------------------------------------------------------*/
/* board_rules_legal_revive_target                                          */
/*--------------------------------------------------------------------------*/

int board_rules_legal_revive_target(BOARD_STATE *state, 
				    int master_x, int master_y, int x, int y)
{
   return (board_rules_verify_revive_target(state, master_x, master_y, x, y) 
           == BOARD_MOVE_OK);
}

/*--------------------------------------------------------------------------*/
/* board_rules_next_ground_step                                             */
/*--------------------------------------------------------------------------*/

/* return the distance needed for the ground creature at ax, ay to
 * travel from x0, y0 to x1, y1
 * if there is no route, return 0
 *
 * NOTE: this function will happily find the distance to an illegal
 * target. You must check the validity of the target elsewhere. */

int board_rules_next_ground_step(BOARD_STATE *state, int ax, int ay,
                                 int x0, int y0, int x1, int y1)
{
   int dir = 0;
   /* check/fill the cache */
   board_rules_calc_ground_routes(state, ax, ay);
   
   /* step backwards through the cached route *
    * until we reach the current location */
   while (x1 != x0 || y1 != y0) {
      dir = state->routes.dir[y1][x1];
      if (dir == 0)
         break;
      y1 += state_move_y_step[state_opposite[dir]];
      x1 += state_move_x_step[state_opposite[dir]];
   }
   if (dir != 0)
      return dir;

   /* if we got this far, something is probably wrong. *
    * We got all the way back to ax, ay without reaching our *
    * current location. Step towards ax, ay - all roads lead from there. */
   return state_opposite[state->routes.dir[y0][x0]];
}

/*--------------------------------------------------------------------------*/
/* board_rules_in_fly_range                                                 */
/*--------------------------------------------------------------------------*/

int board_rules_in_fly_range(BOARD_STATE *state, int ax, int ay, 
			     int x1, int y1)
{
   int dist = vmax(vabs(x1 - ax), vabs(y1 - ay));
   int range = board_rules_actor_range(state, ax, ay);
   return board_rules_in_range(state, x1, y1) && dist <= range;
}

/*--------------------------------------------------------------------------*/
/* board_rules_calc_ground_routes                                           */
/*--------------------------------------------------------------------------*/

void board_rules_calc_ground_routes(BOARD_STATE *state, int x0, int y0)
{
   if (x0 == state->routes.ax0 && y0 == state->routes.ay0)
      return; /* already cached */

   board_rules_init_ground_routes(state, x0, y0);
   board_rules_search_ground_routes(state);
   board_rules_finalize_ground_routes(state);
}

/*--------------------------------------------------------------------------*/
/* board_rules_init_ground_routes                                           */
/*--------------------------------------------------------------------------*/

void board_rules_init_ground_routes(BOARD_STATE *state, int x0, int y0)
{
   int x, y;
   int min_dist;
   int range = board_rules_actor_range(state, x0, y0);
   
   state->routes.ax0 = x0;
   state->routes.ay0 = y0;

   for (y = 0; y < board_rules_height(state); y++)
      for (x = 0; x < board_rules_width(state); x++) {
         state->routes.dir[y][x] = 0;

         min_dist = vabs(x - x0) + vabs(y - y0);
         if (min_dist == 0 || min_dist > range)
            state->routes.dist[y][x] = 0;
         else
            state->routes.dist[y][x] = range + 1;
      }
}

/*--------------------------------------------------------------------------*/
/* board_rules_search_ground_routes                                         */
/*--------------------------------------------------------------------------*/

void board_rules_search_ground_routes(BOARD_STATE *state)
{
   int x0, y0, x1, y1, dir, next_dist;
   ROUTE_SEARCH_QUEUE q;
   
   route_search_queue_init(&q);
   route_search_queue_push(&q, state->routes.ax0, state->routes.ay0);
   
   /* do a breadth-first search of all the cells our actor can visit */
   while (route_search_queue_shift(&q, &x0, &y0)) {
      for (dir = STATE_MOVE_FIRST; dir <= STATE_MOVE_AXIAL_LAST; dir++) {
         next_dist = state->routes.dist[y0][x0] + 1;
         x1 = x0 + state_move_x_step[dir];
         y1 = y0 + state_move_y_step[dir];

         /* if this is the shortest distance to x1, y1 found, record it */
         if (board_rules_in_range(state, x1, y1) &&
             state->routes.dist[y1][x1] > next_dist) {
            state->routes.dist[y1][x1] = next_dist;
            state->routes.dir[y1][x1] = dir;

            /* if x1, y1 is not obstructed, search its neighbors too */
            if (board_rules_empty(state, x1, y1))
               route_search_queue_push(&q, x1, y1);
         }
      }
   }
   /* this should be redundant */
   route_search_queue_clear(&q);
}

/*--------------------------------------------------------------------------*/
/* board_rules_finalize_ground_routes                                       */
/*--------------------------------------------------------------------------*/

/* this is not needed unless we have to bring back the ground_distance */
/* function. However, I don't want anyone to go forgetting about this step: */
/* if a cell is unreachable because of obstacles, its distance value in the */
/* cache could be left at (range + 1). To reproduce the function of */
/* ground_distance, we must return 0, not range+1. */

void board_rules_finalize_ground_routes(BOARD_STATE *state)
{
   int x, y;
   int range = board_rules_actor_range(state, 
                                       state->routes.ax0, 
                                       state->routes.ay0);

   for (y = 0; y < board_rules_height(state); y++) {
      for (x = 0; x < board_rules_width(state); x++) {
         if (state->routes.dist[y][x] > range)
            state->routes.dist[y][x] = 0;
      }
   }
}

/*--------------------------------------------------------------------------*/
/* methods for CELL                                                         */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* cell_init                                                                */
/*--------------------------------------------------------------------------*/

void cell_init(CELL *cell, int actor_num)
{
   cell->actor = malloc(sizeof(ACTOR));
   actor_init(cell->actor, actor_num);
}

/*--------------------------------------------------------------------------*/
/* cell_copy                                                                */
/*--------------------------------------------------------------------------*/

void cell_copy(CELL *cell, CELL *orig)
{
   cell->flags = orig->flags;
   cell->rocks = orig->rocks;  /* is this dangerous? */
   if (orig->actor == NULL)
      cell->actor = NULL;
   else {
      cell->actor = malloc(sizeof(ACTOR));
      actor_copy(cell->actor, orig->actor);
   }
}

/*--------------------------------------------------------------------------*/
/* cell_lumi                                                                */
/*--------------------------------------------------------------------------*/

int cell_lumi(CELL *cell, int board_lumi)
{
   if (cell->flags & CELL_DARK)
      return CELL_DARK | LUMI_DARKEST;
   if (cell->flags & CELL_LIGHT)
      return CELL_LIGHT | LUMI_LIGHTEST;
   if (board_lumi < LUMI_COUNT / 2)
      return CELL_DARK | board_lumi;
   else
      return CELL_LIGHT | board_lumi;
}

/*--------------------------------------------------------------------------*/
/* cell_clear                                                               */
/*--------------------------------------------------------------------------*/

void cell_clear(CELL *cell)
{
   actor_clear(cell->actor);
   free(cell->actor);
   cell->actor = NULL;
   cell->flags &= ~CELL_IMPRISON;
}

/*--------------------------------------------------------------------------*/
/* methods for COMMAND                                                      */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* command_copy                                                             */
/*--------------------------------------------------------------------------*/

void command_copy(COMMAND *cmd, COMMAND orig)
{
   cmd->b.spell=orig.b.spell;
   cmd->b.ax0=orig.b.ax0;
   cmd->b.ay0=orig.b.ay0;
   cmd->b.sx0=orig.b.sx0;
   cmd->b.sy0=orig.b.sy0;
   cmd->b.x1=orig.b.x1;
   cmd->b.y1=orig.b.y1;
}

/*--------------------------------------------------------------------------*/
/* methods for ROUTE_SEARCH_QUEUE                                           */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* route_search_queue_init                                                  */
/*--------------------------------------------------------------------------*/

void route_search_queue_init(ROUTE_SEARCH_QUEUE *q)
{
   q->front = q->back = NULL;
}

/*--------------------------------------------------------------------------*/
/* route_search_queue_clear                                                 */
/*--------------------------------------------------------------------------*/

void route_search_queue_clear(ROUTE_SEARCH_QUEUE *q)
{
   int x, y;

   while (q->front != NULL) {
      route_search_queue_shift(q, &x, &y);
   }
}

/*--------------------------------------------------------------------------*/
/* route_search_queue_push                                                  */
/*--------------------------------------------------------------------------*/

void route_search_queue_push(ROUTE_SEARCH_QUEUE *q, int x, int y)
{
   ROUTE_SEARCH_NODE *new_back = malloc(sizeof(ROUTE_SEARCH_NODE));
   new_back->x = x;
   new_back->y = y;
   new_back->next = NULL;

   if (q->back == NULL) {
      q->front = new_back;
      q->back = new_back;
   }
   else {
      q->back->next = new_back;
      q->back = new_back;
   }
}

/*--------------------------------------------------------------------------*/
/* route_search_queue_shift                                                 */
/*--------------------------------------------------------------------------*/

/* put the x, y value of the front node into the ref parameters. */
/* return a true value if the queue was not empty */

int route_search_queue_shift(ROUTE_SEARCH_QUEUE *q, int *x_ref, int *y_ref)
{
   ROUTE_SEARCH_NODE *old_front = q->front;

   if (old_front == NULL)
      return 0;

   *x_ref = old_front->x;
   *y_ref = old_front->y;

   q->front = old_front->next;
   if (q->front == NULL)
      q->back = NULL;

   free(old_front);

   return 1;
}
