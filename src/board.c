/*--------------------------------------------------------------------------*/
/* game board                                                               */
/*--------------------------------------------------------------------------*/

#include <config.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "board.h"
#include "field.h"
#include "sprite.h"
/* #include "canvas.h" */
#include "iface.h"
#include "audio.h"

/*--------------------------------------------------------------------------*/
/* defines                                                                  */
/*--------------------------------------------------------------------------*/

#define ANIMATE_FRAMES  4               /* animate the board once in this */

/* many frames (a power of 2) */

/*--------------------------------------------------------------------------*/
/* variables                                                                */
/*--------------------------------------------------------------------------*/

/* move to board_new or whatever the 'constructor' will be
static BOARD_DATA board = { 0 };               
game status == none */

/*--------------------------------------------------------------------------*/
/* spells                                                                   */
/*--------------------------------------------------------------------------*/

static char *spell_names[SPELL_COUNT_2] = {
   NULL, "teleport", "heal", "shift time", "exchange", "summon elemental",
   "revive", "imprison", "cease conjuring", NULL
};

static char *elem_names[] = { "an air", "an earth", "a fire", "a water" };

/*--------------------------------------------------------------------------*/
/* public functions                                                         */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* board_init                                                               */
/*--------------------------------------------------------------------------*/

void board_init(BOARD_DATA *board)
{
   int i;
   board_rules_init(&board->state);
   board_canvas_init(&board->canvas);
   board_iface_init(&board->iface);
  
   board->game_status = 0;
   board->select_what = SELECT_SOURCE;
   board->cx = board->cy = board->cstate = 0;
   board->cmove = CMOVE_ANIMATE;
   board->cmd.b.ax0 = board->cmd.b.ay0 = 0;
   board->cmd.b.spell = 0;
   board->cmd.b.sx0 = board->cmd.b.sy0 = 0;
   board->cmd.b.x1 = board->cmd.b.x1 = 0;
   board->frame_time = 0;
   for (i=0; i<=1; i++)
      board->side_name[i] = "";
}

/*--------------------------------------------------------------------------*/
/* board_find_actor                                                         */
/*--------------------------------------------------------------------------*/

int board_find_actor(BOARD_DATA *board, int type, int *ax, int *ay)
{
   return board_rules_find_actor(&board->state, type, ax, ay);
}

/*--------------------------------------------------------------------------*/
/* board_start_game                                                         */
/*--------------------------------------------------------------------------*/

void board_start_game(BOARD_DATA *board, int game, int light_first)
{
   board->game_status = 1;               /* game on! */
   board->frame_time = 0;
   board_rules_start_game(&board->state, game, light_first);
   board_canvas_start_game(&board->canvas, 
                           board_rules_width(&board->state),
                           board_rules_height(&board->state));
   switch (game) {
   case GAME_ARCHON:
      board->side_name[0] = "the light side"; /* canvas? */
      board->side_name[1] = "the dark side";
      break;
   case GAME_ADEPT:
      board->side_name[0] = "chaos";
      board->side_name[1] = "order";
      break;
   }
   /* is this the right way to get the message onscreen? */
   board_start_game_message(board);
   audio_start_game();
   /*  sleep(2); * where is sleep declared as a function? I get errors */

   board_start_turn(board, 1); /* starting a turn is the same as ending one */

   board_refresh(board);
}

/*--------------------------------------------------------------------------*/
/* board_frame                                                              */
/*--------------------------------------------------------------------------*/

int board_frame(BOARD_DATA *board)
{
   int dir;

#ifdef AUTOPILOT
   if (board_rules_turn(&board->state) == 1000) {
      printf("board:  game is taking too long, ending it.\n");
      board_end_game(board);
   }
#endif

   if (board_animate_board(board) == READY_FOR_UPDATE) {
      board_update_state(board);

      if (board_input_ready(board)) {
         iface_frame();

         if (board_iface_select(&board->iface)) {
#ifdef DEBUG_BOARD            
            printf("board_frame: selection made\n");
#endif
            board->action_result = board_selection(board);
#ifdef DEBUG_BOARD
            printf("board_frame: result = %d\n", board->action_result);
#endif
         }
         else {
            dir = board_iface_dir(&board->iface, board->cmove);
            if (dir) {
#ifdef DEBUG_BOARD               
               printf("board_frame: dir %d chosen\n", dir);
#endif
               board->action_result = board_step(board, dir);
#ifdef DEBUG_BOARD
               printf("board_frame: result = %d\n", board->action_result);
#endif
            }
         }
         board_message(board, board->action_result);   
      }
   }

   return (board->select_what != SELECT_GAME_OVER);
}

/*--------------------------------------------------------------------------*/
/* board_pause_game                                                         */
/*--------------------------------------------------------------------------*/

int board_pause_game(BOARD_DATA *board, int pause)
{
   if (board->game_status == 0)
      return 0;
   if (pause != -1)
      board->game_status = pause ? -1 : 1;
   return 1;
}

/*--------------------------------------------------------------------------*/
/* board_refresh                                                            */
/*--------------------------------------------------------------------------*/

void board_refresh(BOARD_DATA *board)
{
   int x, y;

   if (board->select_what == SELECT_FIELD) {
      field_refresh();
      return;
   }

   canvas_clear();
   for (y = 0; y < board_rules_height(&board->state); y++)
      for (x = 0; x < board_rules_width(&board->state); x++)
         board_paint_cell(board, x, y);
   /*  board_message(NULL); */
   board_paint_cursor(board);
   canvas_refresh();
}

/*--------------------------------------------------------------------------*/
/* board_next_step                                                          */
/*--------------------------------------------------------------------------*/

int board_next_step(BOARD_DATA *board, COMMAND cmd)
{
   int x0, y0, x_target, y_target;

   if (!board_command_matches(board, cmd))
      return 0;

   x_target = board_ctarget_cell_x(board, cmd);
   y_target = board_ctarget_cell_y(board, cmd);
   x0 = board_cursor_cell_x(board);
   y0 = board_cursor_cell_y(board);

   if (x0 == x_target && y0 == y_target)
     return STATE_FIRE;

  if (board_cursor_appearance(board) != CURSOR_ACTOR)
     return state_towards(x_target, y_target, x0, y0);
  else {
     return board_rules_next_actor_step(&board->state, 
                                        board->cmd.b.ax0, board->cmd.b.ay0,
                                        x0, y0, x_target, y_target);
  }
}

/*--------------------------------------------------------------------------*/
/* board_move_complete                                                      */
/*--------------------------------------------------------------------------*/

int board_move_complete(BOARD_DATA *board, COMMAND cmd, int key)
{
   /* the only way to complete a move should be to hit fire. */
   /* so simulate a selection here. */

   int result;

   if (key != STATE_FIRE)
      return 0;

   if (!board_command_matches(board, cmd))
      return 0;

   result = board_verify_selection(board);

   return (result == BOARD_MOVE_OK || result == BOARD_NEED_FIELD);
}

/*--------------------------------------------------------------------------*/
/* board_absolute_control_delta                                             */
/*--------------------------------------------------------------------------*/

void board_absolute_control_delta(BOARD_DATA *board, int *dx, int *dy)
{
   /*  int spell; */

   if (board->cmove == CMOVE_STEP) {
      *dx = 0;
      /* don't just hold the direction down, or the selection will get
       * stuck on the first option */
      if (board_iface_menudir_held(&board->iface))
         *dy = 0;
      else {
         *dy = (*dy/board_cursor_cell_ysize(board)) - board->cy;
         *dy -= board->cy;
         /* is that equivalent to this?
            for (spell = 0; spell < board.spell; spell++)
            if (spell_avails[board.side][spell])
            (*dy)--;
         */
      }
   }
   else {
      *dx -= CELL_XOFFSET(*dx) + board_cursor_pix_x(board);
      *dy -= CELL_YOFFSET(*dy) + board_cursor_pix_y(board);
   }
}

/*--------------------------------------------------------------------------*/
/* board_side                                                               */
/*--------------------------------------------------------------------------*/

int board_side(BOARD_DATA *board)
{
  return board_rules_side(&board->state);
}

/*--------------------------------------------------------------------------*/
/* board_elem_num                                                           */
/*--------------------------------------------------------------------------*/

int board_elem_num(BOARD_DATA *board)
{
   return board_rules_elem_num(&board->state, board->frame_time % 4);
}

/*--------------------------------------------------------------------------*/
/* private functions                                                        */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* board_init_field                                                         */
/*--------------------------------------------------------------------------*/

void board_init_field(BOARD_DATA *board, int xd, int yd)
{
   CELL *field_cell;
   ACTOR *attacker;
   ACTOR *defender;
   int pix_x = board_canvas_pix_x(&board->canvas, xd);
   int pix_y = board_canvas_pix_y(&board->canvas, yd);

   board->cmd.b.x1 = xd;
   board->cmd.b.y1 = yd;
   field_cell = board_rules_field_cell(&board->state, board->cmd);
   attacker = board_rules_attacker(&board->state, board->cmd);
   defender = board_rules_defender(&board->state, board->cmd);

   /* what is this for? */
   field_cell->flags &= ~CELL_LUMI_MASK;
   if (field_cell->flags & CELL_LIGHT)
      field_cell->flags |= FLOOR_LIGHT;
   if (field_cell->flags & CELL_DARK)
      field_cell->flags |= FLOOR_DARK;
   if (field_cell->flags & CELL_LUMI)
      field_cell->flags |= board_rules_lumi(&board->state);

   actor_reset(attacker);
   actor_reset(defender);

   if (actor_is_side(attacker, 0))
      field_start_game(board, attacker, defender, field_cell, pix_x, pix_y);
   else
      field_start_game(board, defender, attacker, field_cell, pix_x, pix_y);

   board->select_what = SELECT_FIELD;
}

/*--------------------------------------------------------------------------*/
/* board_field                                                              */
/*--------------------------------------------------------------------------*/

/* return 0 if a field frame was output (the battle is continuing)
 * update the game state and return 1 if the battle is over */

int board_field(BOARD_DATA *board)
{
   ACTOR *winner;

   winner = field_frame();
   if (winner != NULL) {
      if (winner == board_rules_attacker(&board->state, board->cmd))
         board->action_result = BOARD_FIELD_ATTACKER_WIN;
      else if (winner == board_rules_defender(&board->state, board->cmd))
         board->action_result = BOARD_FIELD_DEFENDER_WIN;
      else
         board->action_result = BOARD_FIELD_BOTH_LOSE;
      return READY_FOR_UPDATE;
   }
   return NOT_READY_FOR_UPDATE;
}

/*--------------------------------------------------------------------------*/
/* board_input_ready                                                        */
/*--------------------------------------------------------------------------*/

int board_input_ready(BOARD_DATA *board)
{
   return (board->cstate == 0) && board_canvas_ssp_done(&board->canvas) &&
      board->select_what != SELECT_GAME_OVER;
}

/*--------------------------------------------------------------------------*/
/* board_update_state                                                       */
/*--------------------------------------------------------------------------*/

void board_update_state(BOARD_DATA *board)
{
   int x1 = board_cursor_cell_x(board);
   int y1 = board_cursor_cell_y(board);
  
   switch (board->action_result) {
   case BOARD_NEED_TARGET:
      board->select_what = SELECT_TARGET;
      break;
   case BOARD_NEED_SPELL:
      board_init_spell(board);
      break;
   case BOARD_NEED_SPELL_SOURCE:
      board_init_spell_source(board);
      break;
   case BOARD_NEED_SPELL_TARGET:
      board_init_spell_target(board);
      break;
   case BOARD_NEED_FIELD:
      board_init_field(board, x1, y1);
      break;
   case BOARD_CANCEL:
   case BOARD_POWER_POINT:
      board_start_turn(board, 0);
      break;
   case BOARD_MOVE_OK:
      board_make_move(board, x1, y1);
      break;
   case BOARD_FIELD_ATTACKER_WIN:
   case BOARD_FIELD_DEFENDER_WIN:
   case BOARD_FIELD_BOTH_LOSE:
      board_field_done(board, board->action_result);
   }
   board_set_cmove(board);

   board->action_result = 0;
}

/*--------------------------------------------------------------------------*/
/* board_init_spell                                                         */
/*--------------------------------------------------------------------------*/

void board_init_spell(BOARD_DATA *board)
{
   /* is this still needed?
      actor_reset(board_rules_actor(&board->state, board->cmd.b.ax0, board->cmd.b.ay0));
      board_paint_cell(board, board->cmd.b.ax0, board->cmd.b.ay0);
   */
   board->select_what = SELECT_SPELL;
   board->cx = board_cursor_min_x(board);
   board->cy = board_cursor_min_y(board);
}

/*--------------------------------------------------------------------------*/
/* board_init_spell_source                                                  */
/*--------------------------------------------------------------------------*/

void board_init_spell_source(BOARD_DATA *board)
{
   board->select_what = SELECT_SPELL_SOURCE;

   if (board->cmd.b.spell == SPELL_REVIVE) {
      board_paint_revive_options(board);
      /* clear revive options all other steps? */    
      board->cx = board_cursor_min_x(board);
      board->cy = board_cursor_min_y(board);
   }
   else if (board->cmd.b.spell == SPELL_SUMMON_ELEMENTAL) {
      fprintf(stderr, "board.c/board_init_spell_source(): It is illegal to
 select an elemental!");
      exit(EXIT_FAILURE);
   }
   else {
      board->cx = board->cmd.b.ax0 * board_cursor_cell_xsize(board);
      board->cy = board->cmd.b.ay0 * board_cursor_cell_ysize(board);
   }
}

/*--------------------------------------------------------------------------*/
/* board_init_spell_target                                                  */
/*--------------------------------------------------------------------------*/

void board_init_spell_target(BOARD_DATA *board)
{
   board->select_what = SELECT_SPELL_TARGET;

   switch (board->cmd.b.spell) {
   case SPELL_TELEPORT:
   case SPELL_EXCHANGE:
      /* no action needed */
      break;
   case SPELL_SUMMON_ELEMENTAL:
   case SPELL_REVIVE:
      board->cx = board->cmd.b.ax0 * CELL_XSIZE;
      board->cy = board->cmd.b.ay0 * CELL_YSIZE;
      break;
   default:
      /* we should never get here */
      break;
   }
}

/*--------------------------------------------------------------------------*/
/* board_set_cmove                                                          */
/*--------------------------------------------------------------------------*/

/* should this be used to set the location of the cursor? That seems to */
/* happen in lots of random places right now. */

void board_set_cmove(BOARD_DATA *board)
{
   switch (board->select_what) {
   case SELECT_SOURCE:
   case SELECT_TARGET:
   case SELECT_SPELL_TARGET:
      board->cmove = CMOVE_ANIMATE;
      break;
   case SELECT_SPELL:
      board->cmove = CMOVE_STEP;
      break;
   case SELECT_SPELL_SOURCE:
      switch (board->cmd.b.spell) {
      case SPELL_TELEPORT:
      case SPELL_HEAL:
      case SPELL_EXCHANGE:
      case SPELL_IMPRISON:
         board->cmove = CMOVE_ANIMATE;
         break;
      case SPELL_REVIVE:
         board->cmove = CMOVE_STEP;
         break;
      default:
         /* we should never get here */
         break;
      }
   default:
      /* or here */
      break;
   }
   if (board->cmove == CMOVE_ANIMATE) 
      board_iface_reset(&board->iface);
}

/*--------------------------------------------------------------------------*/
/* board_make_move                                                          */
/*--------------------------------------------------------------------------*/

void board_make_move(BOARD_DATA *board, int x, int y)
{
   int result;

   /* these are also set in commit_target. do we need it in both? */
   board->cmd.b.x1 = x;
   board->cmd.b.y1 = y;

   result = board_rules_make_move(&board->state, board->cmd);
   if (result == BOARD_GAME_OVER)
      board_end_game(board);
   else
      board_start_turn(board, BOARD_RESULT_OK(result));
   /* the result should always be success. Should we check? */
}

/*--------------------------------------------------------------------------*/
/* board_field_done                                                         */
/*--------------------------------------------------------------------------*/

void board_field_done(BOARD_DATA *board, int attacker_result)
{
   int result;
   result = board_rules_field_winner(&board->state, 
                                     board->cmd, attacker_result);
   if (result == BOARD_GAME_OVER)
      board_end_game(board);
   else
      board_start_turn(board, BOARD_RESULT_OK(result));
   /* the result should always be success. Should we check? */
}

/*--------------------------------------------------------------------------*/
/* board_start_turn                                                         */
/*--------------------------------------------------------------------------*/

void board_start_turn(BOARD_DATA *board, int is_new_side)
{
   int side;
  
   side = board_side(board);

   board->select_what = SELECT_SOURCE;
   board->action_result = BOARD_NEED_SOURCE;
   board_set_cmove(board);

   if (is_new_side) {                     /* otherwise just reset this turn */
      board->cx = (side == 0) ? 
         board_cursor_min_x(board) : board_cursor_max_x(board);
      board->cy = CELL_Y(board_rules_height(&board->state) / 2);
      audio_start_turn(side);
   }
   else {
      board->cx = board->cmd.b.ax0 * board_cursor_cell_xsize(board);
      board->cy = board->cmd.b.ay0 * board_cursor_cell_ysize(board);
   }
   board->cstate = 0;

   board->cmd.b.ax0 = -1;
   board->cmd.b.spell = 0;

   board_refresh(board);
   board_canvas_start_turn(&board->canvas, side);
   
   iface_turn(side, IFACE_BOARD);
}

/*--------------------------------------------------------------------------*/
/* board_end_game                                                           */
/*--------------------------------------------------------------------------*/

int board_end_game(BOARD_DATA *board)
{
   if (board->game_status == 0)
      return 0;

   if (board->select_what == SELECT_FIELD)
      field_end_game();

   /* display end game message */
   audio_end_game(board_rules_winner(&board->state));
   audio_terminate();

   board->game_status = 0;               /* game ends */
   board->select_what = SELECT_GAME_OVER;
   board_rules_clear(&board->state);
   return 1;
}

/*--------------------------------------------------------------------------*/
/* board_step                                                               */
/*--------------------------------------------------------------------------*/

int board_step(BOARD_DATA *board, int dir)
{
   int result;
   int orig_dir = dir;

   result = board_verify_step(board, dir);

   if (!BOARD_RESULT_OK(result) && state_is_diagonal(orig_dir)) {
      dir = state_y_direction(orig_dir);
      result = board_verify_step(board, dir);
   }

   if (!BOARD_RESULT_OK(result) && state_is_diagonal(orig_dir)) {
      dir = state_x_direction(orig_dir);
      result = board_verify_step(board, dir);
   }

   if (result == BOARD_STEP_OK) 
      board_update_cursor(board, dir);

   return result;
}

/*--------------------------------------------------------------------------*/
/* board_verify_step                                                        */
/*--------------------------------------------------------------------------*/

int board_verify_step(BOARD_DATA *board, int dir)
{   
   int x = board_cursor_cell_x(board);
   int y = board_cursor_cell_y(board);
   int result;
   ACTOR *actor;

   switch (board->select_what) {
   case SELECT_SOURCE:
      result = board_verify_cursor_step(board, dir);
      break;
   case SELECT_TARGET:
      result = board_rules_verify_actor_step(&board->state, 
                                             board->cmd.b.ax0, 
                                             board->cmd.b.ay0, x, y, dir);
      /* this is a side effect. Should it be taken out of here? */
      if (!BOARD_RESULT_OK(result) && result != BOARD_OPPOSED) {
         actor = board_rules_actor(&board->state, 
                                   board->cmd.b.ax0, board->cmd.b.ay0);
         sprite_set_state(actor->sprite, dir, 0);
      }
      break;
   case SELECT_SPELL:
      result = board_verify_menu_step(board, dir); 
      break;
   case SELECT_SPELL_SOURCE:
      result = board_verify_spell_source_step(board, dir);
      break;
   case SELECT_SPELL_TARGET:
      result = board_verify_spell_target_step(board, dir);
      break;
   default:
      result = 0;
      /* we should never get here */
   }
   return result;
}

/*--------------------------------------------------------------------------*/
/* board_verify_cursor_step                                                 */
/*--------------------------------------------------------------------------*/

int board_verify_cursor_step(BOARD_DATA * board, int dir)
{
   int x = board_cursor_cell_x(board) + state_move_x_step[dir];
   int y = board_cursor_cell_y(board) + state_move_y_step[dir];
  
   if (board_rules_in_range(&board->state, x, y))
      return BOARD_STEP_OK;
   else
      return BOARD_OUT_OF_RANGE;

   /* do we need to check anything else? */
}

/*--------------------------------------------------------------------------*/
/* board_verify_menu_step                                                   */
/*--------------------------------------------------------------------------*/

int board_verify_menu_step(BOARD_DATA * board, int dir)
{
   /* menus will wrap, so all vertical movement is acceptable */
   if (dir == STATE_MOVE_UP || dir == STATE_MOVE_DOWN)
      return BOARD_STEP_OK;
   else
      return BOARD_NO_MOVE;
}

/*--------------------------------------------------------------------------*/
/* board_verify_spell_source_step                                           */
/*--------------------------------------------------------------------------*/

int board_verify_spell_source_step(BOARD_DATA * board, int dir)
{
   if (board->cmd.b.spell == SPELL_REVIVE)
      return board_verify_revive_source_step(board, dir);

   /* other possible spells are TELEPORT, HEAL, EXCHANGE, IMPRISON */
   else
      return board_verify_cursor_step(board, dir);
}

/*--------------------------------------------------------------------------*/
/* board_verify_spell_target_step                                           */
/*--------------------------------------------------------------------------*/

int board_verify_spell_target_step(BOARD_DATA * board, int dir)
{
   if (board->cmd.b.spell == SPELL_REVIVE)
      return board_verify_revive_target_step(board, dir);

   /* other possible spells are TELEPORT, EXCHANGE, SUMMON ELEMENTAL */
   else
      return board_verify_cursor_step(board, dir);
}

/*--------------------------------------------------------------------------*/
/* board_verify_revive_source_step                                          */
/*--------------------------------------------------------------------------*/

int board_verify_revive_source_step(BOARD_DATA * board, int dir)
{
   int cy;
   int result = board_verify_menu_step(board, dir);
   if (!BOARD_RESULT_OK(result))
      return result;

   cy = board->cy + board_cursor_cell_ysize(board) * state_move_y_step[dir];

   /* not sure if it should be > or >= */
   if (cy < board_cursor_min_y(board) || cy > board_cursor_max_y(board))
      return BOARD_OUT_OF_RANGE;
   else
      return BOARD_STEP_OK;
}

/*--------------------------------------------------------------------------*/
/* board_verify_revive_target_step                                          */
/*--------------------------------------------------------------------------*/

int board_verify_revive_target_step(BOARD_DATA *board, int dir)
{
   int mx = board->cmd.b.ax0;
   int my = board->cmd.b.ay0;
   int x = board_cursor_cell_x(board) + state_move_x_step[dir];
   int y = board_cursor_cell_y(board) + state_move_y_step[dir];

   if (!board_rules_in_revive_target_range(&board->state, mx, my, x, y))
      return BOARD_OUT_OF_RANGE;

   return BOARD_STEP_OK;
}

/*--------------------------------------------------------------------------*/
/* board_update_cursor                                                      */
/*--------------------------------------------------------------------------*/

void board_update_cursor(BOARD_DATA *board, int dir)
{
   board->cstate = dir;
}

/*--------------------------------------------------------------------------*/
/* board_selection                                                          */
/*--------------------------------------------------------------------------*/

int board_selection(BOARD_DATA *board)
{
   int result = 0;
   int commit_result = 0;

   result = board_verify_selection(board);

   if (BOARD_RESULT_OK(result))
      commit_result = board_commit_selection(board);

   return (commit_result ? commit_result : result);
}

/*--------------------------------------------------------------------------*/
/* board_verify_selection                                                   */
/*--------------------------------------------------------------------------*/

int board_verify_selection(BOARD_DATA *board)
{
   int spell = board->cmd.b.spell;
   int x0 = board->cmd.b.ax0;
   int y0 = board->cmd.b.ay0;
   int sx0 = board->cmd.b.sx0;
   int sy0 = board->cmd.b.sy0;
   int x1 = board_cursor_cell_x(board);
   int y1 = board_cursor_cell_y(board);
   int cspell = board_rules_spell_choice(&board->state, y1);
  
   switch (board->select_what) {
   case SELECT_SOURCE:
      return board_rules_verify_source(&board->state, x1, y1);
   case SELECT_TARGET:
      return board_rules_verify_actor_target(&board->state, x0, y0, x1, y1);
   case SELECT_SPELL:
      return board_rules_verify_spell(&board->state, x0, y0, cspell);
   case SELECT_SPELL_SOURCE:
      return board_rules_verify_spell_source(&board->state, spell, x1, y1);
   case SELECT_SPELL_TARGET:
      return board_rules_verify_spell_target(&board->state, spell,
					     x0, y0, sx0, sy0, x1, y1);
   default:
      return BOARD_NO_MOVE; /* we should never get here */
   }
}

/*--------------------------------------------------------------------------*/
/* board_commit_selection                                                   */
/*--------------------------------------------------------------------------*/

int board_commit_selection(BOARD_DATA *board)
{
   int x = board_cursor_cell_x(board);
   int y = board_cursor_cell_y(board);

   switch (board->select_what) {
   case SELECT_SOURCE:
      return board_commit_source(board, x, y);
   case SELECT_TARGET:
      return board_commit_target(board, x, y);
   case SELECT_SPELL:
      return board_commit_spell(board, y);
   case SELECT_SPELL_SOURCE:
      return board_commit_spell_source(board, x, y);
   case SELECT_SPELL_TARGET:
      return board_commit_spell_target(board, x, y);
   default:
      fprintf(stderr, "board.c/board_commit_selection(): Unrecognized board input state %d", board->select_what);
      exit(EXIT_FAILURE);
   }
}

/*--------------------------------------------------------------------------*/
/* board_commit_source                                                      */
/*--------------------------------------------------------------------------*/

int board_commit_source(BOARD_DATA *board, int x, int y)
{
   board->cmd.b.ax0 = x;
   board->cmd.b.ay0 = y;
   return 0;
}

/*--------------------------------------------------------------------------*/
/* board_commit_target                                                      */
/*--------------------------------------------------------------------------*/

int board_commit_target(BOARD_DATA *board, int x, int y)
{
   board->cmd.b.x1 = x;
   board->cmd.b.y1 = y;

   if (board_rules_actor_is_master(&board->state, 
                                   board->cmd.b.ax0, board->cmd.b.ay0))
      board_ssp_init(board, board->cmd.b.ax0, board->cmd.b.ay0, x, y, 0);

   return 0;
}

/*--------------------------------------------------------------------------*/
/* board_commit_spell                                                       */
/*--------------------------------------------------------------------------*/

int board_commit_spell(BOARD_DATA *board, int y)
{
   board->cmd.b.spell = board_rules_spell_choice(&board->state, y);
   if (board->cmd.b.spell == SPELL_SUMMON_ELEMENTAL) {
      /* board_rules is set up to allow the AI to choose an elemental
         when simulating future turns. During actual play, the
         SELECT_SPELL_SOURCE step is skipped using this code to force
         a random elemental choice. */
      board_commit_spell_source(board, 0, board_elem_num(board));
      return BOARD_NEED_SPELL_TARGET;
   }
   return 0;
}

/*--------------------------------------------------------------------------*/
/* board_commit_spell_source                                                */
/*--------------------------------------------------------------------------*/

int board_commit_spell_source(BOARD_DATA *board, int x, int y)
{
   board->cmd.b.sx0 = x;
   board->cmd.b.sy0 = y;
   return 0;
}

/*--------------------------------------------------------------------------*/
/* board_commit_spell_target                                                */
/*--------------------------------------------------------------------------*/

int board_commit_spell_target(BOARD_DATA *board, int x, int y)
{
   /* board->x1 and y1 are never read; cursor position is used instead
      board->x1 = x;
      board->y1 = y;
   */

   switch(board->cmd.b.spell) {
   case SPELL_TELEPORT:
      board_ssp_init(board, board->cmd.b.sx0, board->cmd.b.sy0, x, y, 0);
      break;
   case SPELL_EXCHANGE:
      board_ssp_init(board, board->cmd.b.sx0, board->cmd.b.sy0, x, y, 1);
      break;
      /* how do we start the revive ssp? It doesn't even work right in the 
       * old version ...
       board_ssp_init(cell_x, cell_y, cell_x, cell_y, 0);

       * plus, the revive hasn't actually happened at this point, so
       * there is no actor to draw!
       */
   }
   return 0;
}

/*--------------------------------------------------------------------------*/
/* board_command_matches                                                    */
/*--------------------------------------------------------------------------*/

int board_command_matches(BOARD_DATA * board, COMMAND cmd)
{
  int w = board->select_what;
  
  if (w == SELECT_SOURCE)
    return 1;
  if (cmd.b.ax0 != board->cmd.b.ax0 || cmd.b.ay0 != board->cmd.b.ay0)
    return 0;

  if (w == SELECT_TARGET || w == SELECT_SPELL)
    return 1;
  if (cmd.b.spell != board->cmd.b.spell)
    return 0;

  if (board->cmd.b.spell && board->cmd.b.spell != SPELL_SUMMON_ELEMENTAL) {
    if (w == SELECT_SPELL_SOURCE)
      return 1;
    if (cmd.b.sx0 != board->cmd.b.sx0 || cmd.b.sy0 != board->cmd.b.sy0)
      return 0;
  }

  return 1;
}

/*--------------------------------------------------------------------------*/
/* board_ctarget_cell_x                                                     */
/*--------------------------------------------------------------------------*/

int board_ctarget_cell_x(BOARD_DATA * board, COMMAND cmd)
{
  switch (board->select_what) {
  case SELECT_SOURCE:
    return cmd.b.ax0;
  case SELECT_TARGET:
    if (cmd.b.spell)
      return cmd.b.ax0; /* to cast a spell, click on the master */
    else
      return cmd.b.x1;
  case SELECT_SPELL:
    return 0;
  case SELECT_SPELL_SOURCE:
    /* what about elemental? will we get here? */
    return cmd.b.sx0;
  case SELECT_SPELL_TARGET:
    return cmd.b.x1;
  default:
    /* we should never get here */
    return 0;
  }
}

/*--------------------------------------------------------------------------*/
/* board_ctarget_cell_y                                                     */
/*--------------------------------------------------------------------------*/

int board_ctarget_cell_y(BOARD_DATA * board, COMMAND cmd)
{
  switch (board->select_what) {
  case SELECT_SOURCE:
    return cmd.b.ay0;
  case SELECT_TARGET:
    if (cmd.b.spell)
      return cmd.b.ay0; /* to cast a spell, click on the master */
    else
      return cmd.b.y1;
  case SELECT_SPELL:
    return board_spell_cell_y(board, cmd.b.spell);
  case SELECT_SPELL_SOURCE:
    /* what about elemental? will we get here? */
    return cmd.b.sy0;
  case SELECT_SPELL_TARGET:
    return cmd.b.y1;
  default:
    /* we should never get here */
    return 0;
  }
}

/*--------------------------------------------------------------------------*/
/* board_spell_cell_y                                                       */
/*--------------------------------------------------------------------------*/

int board_spell_cell_y(BOARD_DATA * board, int spell)
{
  return board_rules_spell_index(&board->state, spell);
}

/*--------------------------------------------------------------------------*/
/* board_animate_board                                                      */
/*--------------------------------------------------------------------------*/

/* return true if all animations are complete 
 * and the board is ready for update */

int board_animate_board(BOARD_DATA * board)
{
   int x, y;
   int ready = 1;

   if (board->game_status != 1)
      return 0;

   board->frame_time++;

   if (board->select_what == SELECT_FIELD) {
      return board_field(board);
   }

   if (!board_canvas_ssp_done(&board->canvas)) {
      board_canvas_ssp_frame(&board->canvas);
      ready = 0;
   }
   
   for (y = 0; y < board_rules_height(&board->state); y++)
      for (x = 0; x < board_rules_width(&board->state); x++)
         /* only animate the board every n-th frame */
         if ((board_rules_cell_animated(&board->state, x, y) &&
              !(board->frame_time & (ANIMATE_FRAMES - 1)))
             || board_cursor_overlaps(board, x, y)) {
               board_paint_cell(board, x, y);
         }
   board_cursor(board);
   board_canvas_refresh(&board->canvas);
   return (ready && board_input_ready(board));
}

/*--------------------------------------------------------------------------*/
/* board_cursor                                                             */
/*--------------------------------------------------------------------------*/

void board_cursor(BOARD_DATA *board)
{
   /* erase old cursor? */

   if (board->select_what == SELECT_SPELL_SOURCE &&
       board->cmd.b.spell == SPELL_REVIVE) {
      board_canvas_erase_cursor_box(&board->canvas, 
                                    board_cursor_pix_x(board),
                                    board_cursor_pix_y(board));
      board_paint_revive_option(board, board_cursor_cell_y(board));
   }

#ifdef DEBUG_BOARD
   if (board->cstate) {
      printf("board_cursor: cursor is at %d, %d\n", board->cx, board->cy);
      printf("              moving cursor in dir %d\n", board->cstate);
   }
#endif
   board->cx += state_move_x_step[board->cstate] * board_cursor_inc(board);
   board->cy += state_move_y_step[board->cstate] * board_cursor_inc(board);

   /* cstate should never be allowed to move the cursor off the board,
    * but sometimes the cursor is allowed to wrap. */
   if (board->cx > board_cursor_max_x(board)) {
      board->cx = board_cursor_min_x(board);
   }
   if (board->cx < board_cursor_min_x(board))
      board->cx = board_cursor_max_x(board);
   if (board->cy > board_cursor_max_y(board)) {
      board->cy = board_cursor_min_y(board);
   }
   if (board->cy < board_cursor_min_y(board))
      board->cy = board_cursor_max_y(board);

   board_paint_cursor(board);

   if (board->cmove == CMOVE_STEP || 
       (CELL_XOFFSET(board->cx) == 0 && CELL_YOFFSET(board->cy) == 0)) {
      board->cstate = 0;
   }
}

/*--------------------------------------------------------------------------*/
/* board_paint_cell                                                         */
/*--------------------------------------------------------------------------*/

void board_paint_cell(BOARD_DATA *board, int x, int y)
{
   int flags;
   int frame;
   ACTOR *actor;
   int lumi;

   flags = board_rules_cell(&board->state, x, y)->flags;
   frame = board->frame_time/ANIMATE_FRAMES;
   lumi = board_rules_lumi(&board->state);
   actor = board_rules_actor(&board->state, x, y);

   /* if the actor is moving around the board, don't show it */
   /* use board_cursor_appearance? */
   if (board->select_what == SELECT_TARGET && 
       x == board->cmd.b.ax0 && y == board->cmd.b.ay0 && 
       !actor_is_master(actor))
      actor = NULL;

   board_canvas_paint_cell(&board->canvas, x, y, flags, actor, lumi, frame);
}

/*--------------------------------------------------------------------------*/
/* board_paint_revive_options                                               */
/*--------------------------------------------------------------------------*/

void board_paint_revive_options(BOARD_DATA *board)
{
   int i, count;

   count = board_rules_revive_count(&board->state);
   for (i = 0; i < count; i++)
      board_paint_revive_option(board, i);
}

/*--------------------------------------------------------------------------*/
/* board_paint_revive_option                                                */
/*--------------------------------------------------------------------------*/

void board_paint_revive_option(BOARD_DATA *board, int i)
{
   int actor_num, side;

   if (i >= board_rules_revive_count(&board->state))
       return;
   actor_num = board_rules_revive_choice(&board->state, i);
   side = board_side(board);
   board_canvas_paint_revive_option(&board->canvas, actor_num, side, i);
}

/*--------------------------------------------------------------------------*/
/* board_paint_cursor                                                       */
/*--------------------------------------------------------------------------*/

void board_paint_cursor(BOARD_DATA *board)
{
   int sx = 0;
   int sy = 0;

   if (board->select_what == SELECT_GAME_OVER)
      return;

   if (board_cursor_appearance(board) != CURSOR_MESSAGE) {
      sx = board_cursor_pix_x(board);
      sy = board_cursor_pix_y(board);
   }

   if (board_cursor_appearance(board) == CURSOR_ACTOR) {
      board_canvas_paint_actor(&board->canvas, sx, sy, 
                               board_selected_actor(board), board->cstate);
   }

   if (board_cursor_appearance(board) == CURSOR_MESSAGE && 
       board->cstate != 0) 
      board_cursor_message(board, board_cursor_cell_y(board));

   if (board_cursor_appearance(board) != CURSOR_BOX) {
      sx = board_canvas_pix_x(&board->canvas, board->cmd.b.ax0);
      sy = board_canvas_pix_y(&board->canvas, board->cmd.b.ay0);
   }

   board_canvas_paint_cursor_box(&board->canvas, sx, sy, board_side(board));
}

/*--------------------------------------------------------------------------*/
/* board_cursor_message                                                     */
/*--------------------------------------------------------------------------*/

void board_cursor_message(BOARD_DATA *board, int y)
{
   int spell;
   if (board->select_what == SELECT_SPELL) {
      spell = board_rules_spell_choice(&board->state, y);
      board_spell_message(board, spell);
   }
   else {
      fprintf(stderr, "board_cursor_message: can't display message when not in SELECT_SPELL mode.\n");
      fprintf(stderr, "                      (mode=%d)\n", board->select_what);
      exit(EXIT_FAILURE);
   }
}

/*--------------------------------------------------------------------------*/
/* board_spell_message                                                      */
/*--------------------------------------------------------------------------*/

void board_spell_message(BOARD_DATA *board, int spell)
{
   /* spell_names is a global array. */
   /* hopefully it will go out of here when whe have better
   * multi-language, multi-game, or theme support */

   char message[64];
   sprintf(message, "select spell: %s", spell_names[spell]);
   board_canvas_message(&board->canvas, board_rules_lumi(&board->state), 
                        board_rules_lumi_d(&board->state), message);
}

/*--------------------------------------------------------------------------*/
/* board_start_game_message                                                 */
/*--------------------------------------------------------------------------*/

void board_start_game_message(BOARD_DATA *board)
{
   char message[64];
   int side = board_side(board);
   sprintf(message, "%s goes first", board->side_name[side]);
   /* leave out lumi info? */
   board_canvas_message(&board->canvas, board_rules_lumi(&board->state), 
                        board_rules_lumi_d(&board->state), message);
}

/*--------------------------------------------------------------------------*/
/* board_message                                                            */
/*--------------------------------------------------------------------------*/

void board_message(BOARD_DATA *board, int message_code)
{
   char message[64];
   char * side_name;
   ACTOR * actor;
   int spell;

   side_name = board->side_name[board_side(board)];
   actor = board_selected_actor(board);

   switch (message_code) {

   case BOARD_IMPRISONED:
      sprintf(message, "alas, master, this %s is imprisoned", actor->name);
      break;

   case BOARD_CANT_MOVE:
      sprintf(message, "alas, master, this %s cannot move", actor->name);
      break;

   case BOARD_MOVED_LIMIT:
      sprintf(message, "Alas, master, you have moved your limit.");
      break;

   case BOARD_OCCUPIED:
      if (actor_is_ground(actor))
         sprintf(message, "The square ahead is occupied.");
      else
         sprintf(message, "you cannot attack your own");
      break;

   case BOARD_OPPOSED:
      sprintf(message, "Do you challenge this foe?");
      /*    sprintf(message, "You must attack or retreat."); */
      break;

   case BOARD_SPELL_NO_SOURCES:
      spell = board_rules_spell_choice(&board->state,
                                       board_cursor_cell_y(board));
      if (spell == SPELL_IMPRISON)
         sprintf(message, "%s cannot be imprisoned at this time", side_name);
      else if (spell == SPELL_REVIVE)
         sprintf(message, "happily, master, all are alive");
      else {
         /* we should never get here */
         fprintf(stderr, "board_message: No message for BOARD_SPELL_NO_SOURCES with spell %s\n", spell_names[spell]);
         exit(EXIT_FAILURE);
      }
      break;

   case BOARD_SPELL_NO_TARGETS:
      /* assume select spell revive */
      sprintf(message, "There is no open square near your %s.", actor->name);
      break;

   case BOARD_POWER_POINT:
      sprintf(message, "power points block all spells");
      break;

   case BOARD_NEED_SOURCE:
      /*   what if the turn starts over? */
      sprintf(message, "%s plays", side_name);
      break;

   case BOARD_NEED_TARGET:
      sprintf(message, "%s (%s %d)", actor->name, 
              actor_is_ground(actor) 
              ? "ground" 
              : actor_is_fly(actor) ? "fly" : "teleport",
              actor->distance);
      break;

   case BOARD_NEED_SPELL:
      sprintf(message, "the %s conjures a spell", actor->name);
      break;

   case BOARD_NEED_SPELL_SOURCE:
#ifdef DEBUG_BOARD
      printf("board_message: BOARD_NEED_SPELL_SOURCE spell=%d\n",
             board->cmd.b.spell);
#endif
      if (board->cmd.b.spell == SPELL_REVIVE)
         sprintf(message, "select who to revive");
      else if (board->cmd.b.spell != SPELL_SUMMON_ELEMENTAL)
         sprintf(message, "who would you like to %s?", 
                 spell_names[board->cmd.b.spell]);
      else
         return; /* summon elemental does this automatically, so no message */
      break;

   case BOARD_NEED_SPELL_TARGET:
      if (board->cmd.b.spell == SPELL_TELEPORT)
         sprintf(message, "where would you like to teleport it?");
      else if (board->cmd.b.spell == SPELL_EXCHANGE)
         sprintf(message, "whom to exchange with?");
      else if (board->cmd.b.spell == SPELL_SUMMON_ELEMENTAL)
         sprintf(message, "%s elemental appears", 
                 elem_names[board->cmd.b.sy0]);
      else if (board->cmd.b.spell == SPELL_REVIVE)
         sprintf(message, "place it within the charmed square");
      break;

   case BOARD_MOVE_OK:
      if (board->cmd.b.spell == SPELL_SHIFT_TIME)
         sprintf(message, "the flow of time is reversed");
      else if (board->cmd.b.spell == SPELL_HEAL)
         sprintf(message, "this %s is now healed", actor->name);
      else if (board->cmd.b.spell == SPELL_IMPRISON)
         sprintf(message, "this %s is now imprisoned", actor->name);
      else
         return; /* a regular move produces no message */
      break;

   case BOARD_GAME_OVER:
      switch (board_rules_winner(&board->state)) {
      case WINNER_TIE:
         sprintf(message, "The game is over.  The result is a tie!");
      case WINNER_DARK:
         sprintf(message, "The game is over.  The dark side wins!");
      case WINNER_LIGHT:
         sprintf(message, "The game is over.  The light side wins!");
      }
      break;
   default:
      return; /* whatever result happened was silent */
   }
   board_canvas_message(&board->canvas, board_rules_lumi(&board->state), 
                        board_rules_lumi_d(&board->state), message);
}

/*--------------------------------------------------------------------------*/
/* board_ssp_init                                                           */
/*--------------------------------------------------------------------------*/

void board_ssp_init(BOARD_DATA *board, 
		    int x0, int y0, int x1, int y1, int both)
{
   void *sprite1;
   void *sprite2;

   sprite1 = board_rules_actor(&board->state, x0, y0)->sprite;
   if (both)
      sprite2 = board_rules_actor(&board->state, x1, y1)->sprite;
   else
      sprite2 = NULL;

   board_canvas_ssp_init(&board->canvas, 
                         x0, y0, x1, y1, sprite1, sprite2, both);
}

/*--------------------------------------------------------------------------*/
/* board_cursor_overlaps                                                    */
/*--------------------------------------------------------------------------*/

int board_cursor_overlaps(BOARD_DATA *board, int x, int y)
{
   int min_x, min_y, max_x, max_y;

   /* this will sometimes give a false positive */
   /* (eg if the cursor is off the board as spell or revive choice) */
   /* but a false positive doesn't really do any harm */

   min_x = board_cursor_cell_x(board);
   min_y = board_cursor_cell_y(board);

   if (CELL_XOFFSET(board->cx) == 0)
      max_x = min_x;
   else {
      max_x = min_x + 1;
   }

   if (CELL_YOFFSET(board->cy) == 0)
      max_y = min_y;
   else {
      max_y = min_y + 1;
   }
   if (y >= min_y && y <= max_y && x >= min_x && x <= max_x) {
      return 1;
   }
   else
      return 0;
}

/*--------------------------------------------------------------------------*/
/* board_cursor_cell_x                                                      */
/*--------------------------------------------------------------------------*/

int board_cursor_cell_x(BOARD_DATA *board)
{
   return board->cx / board_cursor_cell_xsize(board);
}

/*--------------------------------------------------------------------------*/
/* board_cursor_cell_y                                                      */
/*--------------------------------------------------------------------------*/

int board_cursor_cell_y(BOARD_DATA *board)
{
   return board->cy / board_cursor_cell_ysize(board);
}

/*--------------------------------------------------------------------------*/
/* board_cursor_cell_xsize                                                  */
/*--------------------------------------------------------------------------*/

int board_cursor_cell_xsize(BOARD_DATA *board)
{
   return CELL_XSIZE;
}

/*--------------------------------------------------------------------------*/
/* board_cursor_cell_ysize                                                  */
/*--------------------------------------------------------------------------*/

int board_cursor_cell_ysize(BOARD_DATA *board)
{
   if (board->select_what == SELECT_SPELL_SOURCE && 
       board->cmd.b.spell == SPELL_REVIVE)
      return board_canvas_revive_cell_ysize(&board->canvas);

   if (board->select_what == SELECT_SPELL)
      return 1;

   else
      return CELL_YSIZE;
}

/*--------------------------------------------------------------------------*/
/* board_cursor_inc                                                         */
/*--------------------------------------------------------------------------*/

/* return the number of pixels the cursor moves each frame */

int board_cursor_inc(BOARD_DATA *board)
{
   int cell_ysize = CELL_YSIZE;
   int steps;
  
   if (board->cmove == CMOVE_ANIMATE)
      steps = 8;
   else
      steps = 1;

   if (board->select_what == SELECT_SPELL_SOURCE && 
       board->cmd.b.spell == SPELL_REVIVE)
      cell_ysize = board_canvas_revive_cell_ysize(&board->canvas);

   if (board->select_what == SELECT_SPELL)
      cell_ysize = 1;

   return cell_ysize / steps;
}

/*--------------------------------------------------------------------------*/
/* board_cursor_pix_x                                                       */
/*--------------------------------------------------------------------------*/

int board_cursor_pix_x(BOARD_DATA *board)
{
   int x_origin;

   if (board->select_what == SELECT_SPELL_SOURCE && 
       board->cmd.b.spell == SPELL_REVIVE)
      x_origin = board_canvas_revive_x_origin(&board->canvas, 
					      board_side(board));
   else 
      x_origin = board_canvas_pix_x(&board->canvas, 0);

   return x_origin + board->cx;
}

/*--------------------------------------------------------------------------*/
/* board_cursor_pix_y                                                       */
/*--------------------------------------------------------------------------*/

int board_cursor_pix_y(BOARD_DATA *board)
{
   int y_origin;

   if (board->select_what == SELECT_SPELL_SOURCE && 
       board->cmd.b.spell == SPELL_REVIVE)
      y_origin = board_canvas_revive_y_origin(&board->canvas, 
                                              board_side(board));
   else 
      y_origin = board_canvas_pix_y(&board->canvas, 0);

   return y_origin + board->cy;
}

/*--------------------------------------------------------------------------*/
/* board_cursor_min_x                                                       */
/*--------------------------------------------------------------------------*/

int board_cursor_min_x(BOARD_DATA *board)
{
   return board_cursor_min_cell_x(board) * board_cursor_cell_xsize(board);
}

/*--------------------------------------------------------------------------*/
/* board_cursor_min_y                                                       */
/*--------------------------------------------------------------------------*/

int board_cursor_min_y(BOARD_DATA *board)
{
      return board_cursor_min_cell_y(board) * board_cursor_cell_ysize(board);
}

/*--------------------------------------------------------------------------*/
/* board_cursor_max_x                                                       */
/*--------------------------------------------------------------------------*/

int board_cursor_max_x(BOARD_DATA *board)
{
   return board_cursor_max_cell_x(board) * board_cursor_cell_xsize(board);
}

/*--------------------------------------------------------------------------*/
/* board_cursor_max_y                                                       */
/*--------------------------------------------------------------------------*/

int board_cursor_max_y(BOARD_DATA *board)
{
   return board_cursor_max_cell_y(board) * board_cursor_cell_ysize(board);
}

/*--------------------------------------------------------------------------*/
/* board_cursor_min_cell_x                                                  */
/*--------------------------------------------------------------------------*/

int board_cursor_min_cell_x(BOARD_DATA *board)
{
   if (board->select_what == SELECT_SPELL_TARGET &&
       board->cmd.b.spell == SPELL_REVIVE)
      return vmax(0, board->cmd.b.ax0 - 1);

   return 0;
   /* something different for SELECT_SPELL and select revive source? */
}

/*--------------------------------------------------------------------------*/
/* board_cursor_min_cell_y                                                  */
/*--------------------------------------------------------------------------*/

int board_cursor_min_cell_y(BOARD_DATA *board)
{
   if (board->select_what == SELECT_SPELL_TARGET &&
       board->cmd.b.spell == SPELL_REVIVE)
      return vmax(0, board->cmd.b.ay0 - 1);

   return 0;
   /* something different for SELECT_SPELL and select revive source? */
}

/*--------------------------------------------------------------------------*/
/* board_cursor_max_cell_x                                                  */
/*--------------------------------------------------------------------------*/

int board_cursor_max_cell_x(BOARD_DATA *board)
{
   int max_cell_x;

   if (board->select_what == SELECT_SPELL ||
       (board->select_what == SELECT_SPELL_SOURCE && 
        board->cmd.b.spell == SPELL_REVIVE))
      return 0;

   max_cell_x = (board_rules_width(&board->state) - 1);
   if (board->select_what == SELECT_SPELL_TARGET &&
       board->cmd.b.spell == SPELL_REVIVE)
      max_cell_x = vmin(board->cmd.b.ax0 + 1, max_cell_x);

   return max_cell_x;
}

/*--------------------------------------------------------------------------*/
/* board_cursor_max_cell_y                                                  */
/*--------------------------------------------------------------------------*/

int board_cursor_max_cell_y(BOARD_DATA *board)
{
   int max_board_cell_y = board_rules_height(&board->state) - 1;

   switch (board->select_what) {
   case SELECT_SPELL:
      return board_rules_spell_count(&board->state) - 1;
   case SELECT_SPELL_SOURCE:
      if (board->cmd.b.spell == SPELL_REVIVE) {
         return board_rules_revive_count(&board->state) - 1;
      }
   case SELECT_SPELL_TARGET:
      if (board->cmd.b.spell == SPELL_REVIVE) {
         return vmin(board->cmd.b.ay0 + 1, max_board_cell_y);
      }
   default:
      return max_board_cell_y;
   }
}

/*--------------------------------------------------------------------------*/
/* board_cursor_appearance                                                  */
/*--------------------------------------------------------------------------*/

int board_cursor_appearance(BOARD_DATA *board)
{
   switch (board->select_what) {
   case SELECT_SPELL:
      return CURSOR_MESSAGE;
   case SELECT_TARGET:
      if (!board_rules_actor_is_master(&board->state, 
                                       board->cmd.b.ax0, board->cmd.b.ay0))
         return CURSOR_ACTOR;
   case SELECT_SPELL_TARGET:
      if (board->cmd.b.spell == SPELL_SUMMON_ELEMENTAL)
         return CURSOR_ACTOR;
   default:
      return CURSOR_BOX;
   }
}

/*--------------------------------------------------------------------------*/
/* board_selected_actor                                                     */
/*--------------------------------------------------------------------------*/

ACTOR * board_selected_actor(BOARD_DATA *board)
{
   int x, y;

   if (board->cmd.b.spell == SPELL_SUMMON_ELEMENTAL) {
      return board_rules_elem_actor(&board->state);
   }

   if (board->select_what == SELECT_SOURCE ||
       board->select_what == SELECT_SPELL_SOURCE) {
      x = board_cursor_cell_x(board);
      y = board_cursor_cell_y(board);
   /* if the spell is revive, this won't be meaningful: 
      revive choices aren't really actors.
      But there are no messages involving the name 
      of the actor to revive, 
      and there is another method for painting the actor,
      so this should not be needed.

      perhaps we need an 'offboard' cell as opposed to 
      an 'elemental' cell. Elemental and Revive could 
      both use it. Or perhaps several cells would be needed, for 
      all revive choices.

      - mhc 2002.03.15 */

   }
   else {
      x = board->cmd.b.ax0;
      y = board->cmd.b.ay0;
   }

   return board_rules_actor(&board->state, x, y);
}

/*--------------------------------------------------------------------------*/
/* board_iface_init                                                         */
/*--------------------------------------------------------------------------*/

void board_iface_init(BOARD_IFACE *iface)
{
   iface->fire_down = 0;
   iface->dir_held = 0;
}

/*--------------------------------------------------------------------------*/
/* board_iface_reset                                                        */
/*--------------------------------------------------------------------------*/

void board_iface_reset(BOARD_IFACE *iface)
{
   iface->dir_held = 0;
}

/*--------------------------------------------------------------------------*/
/* board_iface_select                                                       */
/*--------------------------------------------------------------------------*/

int board_iface_select(BOARD_IFACE *iface)
{
   int fire = iface_key_down(STATE_FIRE);

   if (iface->fire_down) {
      iface->fire_down = fire;
      return 0;
   }

   iface->fire_down = fire;
   return fire;
}

/*--------------------------------------------------------------------------*/
/* board_iface_dir                                                          */
/*--------------------------------------------------------------------------*/

int board_iface_dir(BOARD_IFACE *iface, int mode)
{
   int dir;

   for (dir = STATE_MOVE_LAST; dir >= STATE_MOVE_FIRST; dir--)
      if (iface_key_down(dir)) {
#ifdef DEBUG_BOARD
         printf("key down: %d\n", dir);
#endif
         break;
         /* do we need to check for validity here? */
         /* (ground creatures can't use diagonals) */
         /* if so, we could keep looping */
      }

   if (mode == CMOVE_STEP) {
      if (dir == iface->dir_held) 
         return 0;
      else
         iface->dir_held = dir;
   }
   return dir;
}

/*--------------------------------------------------------------------------*/
/* board_iface_menudir_held                                                 */
/*--------------------------------------------------------------------------*/

int board_iface_menudir_held(BOARD_IFACE *iface)
{
   if (iface->dir_held == STATE_MOVE_DOWN || iface->dir_held == STATE_MOVE_UP)
      return iface->dir_held;
   else
      return 0;
}
