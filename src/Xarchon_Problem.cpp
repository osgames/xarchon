#include "Xarchon_Problem.hpp"
#include "Xarchon_Interface.hpp"

Xarchon_State::Xarchon_State(void) 
{
  status=MOVE_COMPLETE;
  board_rules_init(&state);
}

Xarchon_State::Xarchon_State(Xarchon_State *a)
{
#ifdef PROFILE
  static long value=0;
  static int counter=0;
  static struct timeval timeval_s,timeval_e;
  gettimeofday(&timeval_s,NULL);
#endif
  status=a->status;
  command_copy(&cmd, a->cmd);
  board_rules_copy(&state, &a->state);

#ifdef PROFILE
  gettimeofday(&timeval_e,NULL);
  value+=(timeval_e.tv_sec-timeval_s.tv_sec)*1000000+(timeval_e.tv_usec-timeval_s.tv_usec);
  counter++;
  printf ("Xarchon_State *  (%ld,%d)\n",value,counter);
#endif
}

Xarchon_State::~Xarchon_State()
{
  board_rules_clear(&state);
}
  
void Xarchon_State::Get_Real_State(BOARD_STATE *real_state)
{
  status=MOVE_COMPLETE;
#ifdef DEBUG_AI
  printf("Xarchon_State::Get_Real_State: copying board state\n");
#endif
  board_rules_copy(&state, real_state);
}

void Xarchon_State::Start_Game(int game, int light_first)
{
  status=MOVE_COMPLETE;
  board_rules_start_game(&state, game, light_first);
}

// bahh... I oughta move this into actors.c - mhc 2001.11.12
ACTOR *Xarchon_State::Actor_Copy(ACTOR *a)
{
  if (a==NULL)
    return NULL;
  ACTOR *ret=new ACTOR;
  strcpy(ret->name,a->name);
  ret->type=a->type;
  ret->distance=a->distance;
  ret->strength=a->strength;
  ret->weapon=a->weapon;
  ret->recharge=a->recharge;
  return ret;
}

bool Xarchon_State::Fight(void)
{
  return (status == NEED_FIGHT);
}

void Xarchon_State::Field_Statistics(double *p, int *a1, int *d1)
{
  ::Field_Statistics(&state,
                     board_rules_defender(&state, cmd), 
                     board_rules_attacker(&state, cmd),
                     board_rules_field_cell(&state, cmd), p, a1, d1);
}

int Xarchon_State::Height(void)
{
  return board_rules_height(&state);
}

int Xarchon_State::Width(void)
{
  return board_rules_width(&state);
}
 
int Xarchon_State::Side(void)
{
  return board_rules_side(&state);
}

int Xarchon_State::Turn(void)
{
  return board_rules_turn(&state);
}

bool Xarchon_State::Empty(int x, int y)
{
  return board_rules_empty(&state, x, y);
}

ACTOR * Xarchon_State::Actor(int x, int y)
{
  return board_rules_actor(&state, x, y);
}

CELL * Xarchon_State::Cell(int x, int y)
{
  return board_rules_cell(&state, x, y);
}

bool Xarchon_State::Power_Point(int x, int y)
{
  return board_rules_power_point(&state, x, y);
}

int Xarchon_State::Actor_Side(int x, int y)
{
  return board_rules_actor_side(&state, x, y);
}

int Xarchon_State::EndOfGame(void)
{
  if (status != MOVE_COMPLETE)
    return 0;

  /* Winning Bonus */
  /* ************* */

  int winner = board_rules_winner(&state);

  if (winner == WINNER_DARK)
     return -1;
  if (winner == WINNER_LIGHT)
     return 1;
  if (winner == WINNER_TIE)
     return 2; // not used

  return 0;
}

int Xarchon_State::Verify_Source(int x, int y)
{
  return board_rules_verify_source(&state, x, y);
}

int Xarchon_State::Verify_Actor_Target(int ax0, int ay0, int x1, int y1)
{
  return board_rules_verify_actor_target(&state, ax0, ay0, x1, y1);
}

void Xarchon_State::Make_Move(COMMAND new_cmd)
{
#ifdef DEBUG_AI
     printf("Xarchon_State::Make_Move: spell=%d source=(%d,%d) spell source=(%d,%d) target=(%d, %d)\n", 
            new_cmd.b.spell, new_cmd.b.ax0, new_cmd.b.ay0,
            new_cmd.b.sx0, new_cmd.b.sy0, new_cmd.b.x1, new_cmd.b.y1);
#endif
  int result = board_rules_verify_move(&state, new_cmd);
  if (result == BOARD_NEED_FIELD) {
#ifdef DEBUG_AI
     printf("Xarchon_State::Make_Move: need field battle\n");
#endif
     command_copy(&cmd, new_cmd);
     status=NEED_FIGHT;
  }
  else if (result == BOARD_MOVE_OK) {
    board_rules_make_move(&state, new_cmd);
  }
  else {
    ; // we should never get here!
  }
}

void Xarchon_State::Field_Done(bool win, int damage)
{
   // still need to damage the winning actor!
   ACTOR *winner = win ?
      board_rules_attacker(&state, cmd) : board_rules_defender(&state, cmd);
   actor_damage(winner, damage);
   board_rules_field_winner(&state, cmd, win ? 
                            BOARD_FIELD_ATTACKER_WIN : 
                            BOARD_FIELD_DEFENDER_WIN);
   status=MOVE_COMPLETE;
}

Xarchon_Move_Operator::Xarchon_Move_Operator(int s,int x0,int y0,int x1,int y1)
{
  cmd.b.spell=s;
  cmd.b.ax0=x0;
  cmd.b.ay0=y0;
  cmd.b.x1=x1;
  cmd.b.y1=y1;
}

Xarchon_State* Xarchon_Move_Operator::Operate(Xarchon_State *a) 
{ 
  Xarchon_State *newstate=new Xarchon_State(a);
  newstate->Make_Move(cmd);
  return newstate;
}

Xarchon_Fight_Operator::Xarchon_Fight_Operator(bool w,int d)
{
  win=w;
  damage=d;
}

Xarchon_State *Xarchon_Fight_Operator::Operate(Xarchon_State *a)
{
  Xarchon_State *newstate=new Xarchon_State(a);
  newstate->Field_Done(win, damage);
  return newstate;
}


Xarchon_Op_Generator::Xarchon_Op_Generator(void)
{
}

Xarchon_Op_Generator:: ~Xarchon_Op_Generator()
{
}
 
void Xarchon_Op_Generator::Init(Xarchon_State *t) 
{
#ifdef PROFILE
  static long value=0;
  static int counter=0;
  static struct timeval timeval_s,timeval_e;
  gettimeofday(&timeval_s,NULL);
#endif

  batch.clear();
  /* IF there is a fight create fight operators */
  if (t->Fight()) {
    Xarchon_Fight_Operator *win_op=new Xarchon_Fight_Operator(true,5);
    Xarchon_Fight_Operator *lose_op=new Xarchon_Fight_Operator(false,5);
    
    batch.push_front(win_op);
    batch.push_front(lose_op);
    batch_iter=batch.begin();
    return;
  }
  // Add Movements commands
  // Create Move Statements
  Generate_Moves(t);

#ifdef DEBUG_AI
  for (batch_iter=batch.begin();batch_iter!=batch.end();batch_iter++) {
    Operator <Xarchon_State> *dop=*batch_iter;
    Xarchon_Goal_Test gt;
    double val=gt.Goal_Value(dop->Operate(t));
    printf ("value %g\n",val);
  }
#endif

#ifdef DEBUG_AI 
  printf ("Branch_Factor %d\n",batch.size());
#endif

  batch_iter=batch.begin();

#ifdef PROFILE
  gettimeofday(&timeval_e,NULL);
  value+=(timeval_e.tv_sec-timeval_s.tv_sec)*1000000+(timeval_e.tv_usec-timeval_s.tv_usec);
  counter++;
  printf ("Generator (%ld,%d)\n",value,counter);
#endif

}

void Xarchon_Op_Generator::Generate_Moves(Xarchon_State *t)
{
   for (int y=0; y < t->Height(); y++)
      for (int x=0; x < t->Width(); x++)
         if (t->Verify_Source(x, y) == BOARD_NEED_TARGET)
            Generate_Actor_Targets(t, x, y);
}

void Xarchon_Op_Generator::Generate_Actor_Targets(Xarchon_State *t, 
                                                  int ax0, int ay0)
{
  Xarchon_Move_Operator *move_op;  
  for (int y=0; y < t->Height(); y++) // we could limit it to source's range,
    for (int x=0; x < t->Width(); x++) // but would it really be worth it?
      switch (t->Verify_Actor_Target(ax0, ay0, x, y)) {
      case BOARD_MOVE_OK: 
        move_op = new Xarchon_Move_Operator(0,ax0,ay0,x,y);
        batch.push_back(move_op);
        break;
      case BOARD_NEED_FIELD:
        move_op = new Xarchon_Move_Operator(0,ax0,ay0,x,y);
        batch.push_front(move_op);
        break;
      case BOARD_NEED_SPELL:
        // do later!
        break;
      default:
        ; // not a legal move. That's fine.
      }
}    

Operator<Xarchon_State> *Xarchon_Op_Generator::Next(void) 
{
  Operator <Xarchon_State> *cur=*batch_iter;
  batch_iter++;
  return cur;
}

bool Xarchon_Op_Generator::IsEnd(void) 
{
  return batch_iter==batch.end();
}

Xarchon_Goal_Test::Xarchon_Goal_Test(void)
{
  value_parm[LIVE_SCORE]=300;
  value_parm[LIVE_HP_SCORE]=30;
  value_parm[POWERPOINT_SCORE]=200;
  value_parm[WIN_SCORE]=300;
  value_parm[RANDOM_SCORE]=30;
}

double Xarchon_Goal_Test::Goal_Value(Xarchon_State *a) 
{
#ifdef PROFILE
  static long value=0;
  static int counter=0;
  static struct timeval timeval_s,timeval_e;
  gettimeofday(&timeval_s,NULL);
#endif

#ifdef DEBUG_AI
  printf("Xarchon_Goal_Test::Goal_Value\n");
#endif
  /* Calculate Fight value */
  /* ********************* */
  if (a->Fight()) {
    Xarchon_Fight_Goal_Test k(this,this);
    return k.Goal_Value(a);
  }

  double ret_value=0;

  int num_power=0,light_power=0,dark_power=0,total_light=0,total_dark=0;

  /* Calculate Physical Value */
  /* ************************ */
  for (int y=0; y < a->Height(); y++) {
    for (int x=0; x < a->Width(); x++) {
      if (a->Power_Point(x, y))
	num_power++;
      if (!a->Empty(x, y)) {
	ACTOR *actor = a->Actor(x, y);
	int life=field_initial_life(&a->state,actor,a->Cell(x,y),NULL); // How much strength does the actor actually have
	if (actor_is_side(actor,0)) {
	  if (a->Power_Point(x, y)) 
	    light_power++;
	  total_light++;
	  ret_value+=value_parm[LIVE_SCORE];
	  ret_value+=value_parm[LIVE_HP_SCORE]*life;
	}
	if (actor_is_side(actor,1)) {
	  if (a->Power_Point(x, y)) 
	    dark_power++;
	  total_dark++;
	  ret_value-=value_parm[LIVE_SCORE];
	  ret_value-=value_parm[LIVE_HP_SCORE]*life;
	}
      }
    }
  }

  /* Power Points Bonus */
  /* ****************** */
  for (int i=0;i<light_power;i++)
    ret_value+=value_parm[POWERPOINT_SCORE]*i;
  for (int i=0;i<dark_power;i++)
    ret_value-=value_parm[POWERPOINT_SCORE]*i;
  
  /* Winning Bonus */
  /* ************* */

  if (total_light==0 || dark_power==num_power)
    ret_value-=value_parm[WIN_SCORE];
  if (total_dark==0 || light_power==num_power)
    ret_value+=value_parm[WIN_SCORE];

  /* Random Factor */
  /* ************* */
  if (value_parm[RANDOM_SCORE]<0)
    value_parm[RANDOM_SCORE]*=-1;
  int r=(int)value_parm[RANDOM_SCORE];
  if (r==0)
    r=1;
  int random_value=rand()%r;
  ret_value+=random_value;

#ifdef PROFILE
  gettimeofday(&timeval_e,NULL);
  value+=(timeval_e.tv_sec-timeval_s.tv_sec)*1000000+(timeval_e.tv_usec-timeval_s.tv_usec);
  counter++;
  printf ("Xarchon Goal (%ld,%d)\n",value,counter);
#endif

  return ret_value;
}

Xarchon_Fight_Goal_Test::Xarchon_Fight_Goal_Test(Goal_Test <double,Xarchon_State> *w,Goal_Test <double,Xarchon_State> *l)
{
  win=w;
  lose=l;
}

double Xarchon_Fight_Goal_Test::Goal_Value(Xarchon_State *a)
{
  if (a->Fight()) {
#ifdef DEBUG_AI
  printf("Xarchon_Fight_Goal_Test::Goal_Value: fight\n");
#endif
    int al,dl;
    double p;
    a->Field_Statistics(&p,&al,&dl);
    Xarchon_Fight_Operator win_oper  (true,al);
    Xarchon_Fight_Operator lose_oper (false,dl);
    Xarchon_State *win_s=win_oper.Operate(a);
    Xarchon_State *lose_s=lose_oper.Operate(a);
    double win_goal=win->Goal_Value(win_s);
    double lose_goal=lose->Goal_Value(lose_s);
    double ret_value=win_goal*p+lose_goal*(1-p);

    delete win_s;
    delete lose_s;
    return ret_value;
  }
  else {
#ifdef DEBUG_AI
  printf("Xarchon_Fight_Goal_Test::Goal_Value: not fight\n");
#endif
    return win->Goal_Value(a);
  }
}

int Xarchon_Path_Cost::Cost(Xarchon_State *a,Operator<Xarchon_State> *o)
{
  return 0;
}

Xarchon_Problem::Xarchon_Problem(void)
{
  gen=new Xarchon_Op_Generator();
  gt=new Xarchon_Goal_Test();
  pc=new Xarchon_Path_Cost();
}

Xarchon_Problem::~Xarchon_Problem()
{
  delete gen;
  delete gt;
  delete pc;
}

Xarchon_Strategy::Xarchon_Strategy(void)
{
  problem=new Xarchon_Problem;
  oper=NULL;
}

Xarchon_Strategy::~Xarchon_Strategy(void)
{
  delete problem;
}

Operator<Xarchon_State> *Xarchon_Strategy::Next(void)
{
  return oper;
}

void Xarchon_War_Lord::InitState(Xarchon_State *state) 
{
#ifdef DEBUG_AI
   printf("Xarchon_War_Lord::InitState\n");
#endif
  if (!state->Fight()) {
    oper=NULL;
    return;
  }
  double chance;
  int al,dl;
  state->Field_Statistics(&chance,&al,&dl);
  chance=chance *1000;
  int r=(rand()%1000);
#ifdef DEBUG_AI
  printf ("Random %d",r);
#endif
  if (r < (int)chance)
    oper=new Xarchon_Fight_Operator(true,al);
  else
    oper=new Xarchon_Fight_Operator(false,dl);
}

Xarchon_Player_Strategy::Xarchon_Player_Strategy(int d,int b)
{
  depth=d;
  branch=b;
}

Xarchon_Light_Player::Xarchon_Light_Player(int d,int b) : Xarchon_Player_Strategy(d,b)
{
  gt=new Xarchon_Goal_Test;
}

Xarchon_Light_Player::~Xarchon_Light_Player(void)
{
  delete gt;
}

void Xarchon_Light_Player::InitState(Xarchon_State *state) 
{
#ifdef DEBUG_AI
   printf("Xarchon_Light_Player::InitState\n");
#endif
  Xarchon_Light_Accumulator light;
  Xarchon_Goal_Search search(depth,branch,true,&light,gt);
  search.Goal_Value(state);
  oper=light.Get_Operator();
}

Xarchon_Dark_Player::Xarchon_Dark_Player(int d,int b) : Xarchon_Player_Strategy(d,b)
{
  gt=new Xarchon_Goal_Test;
}

Xarchon_Dark_Player::~Xarchon_Dark_Player(void)
{
  delete gt;
}

void Xarchon_Dark_Player::InitState(Xarchon_State *state) 
{
  Xarchon_Dark_Accumulator dark;
#ifdef DEBUG_AI
   printf("Xarchon_Dark_Player::InitState\n");
#endif
  Xarchon_Goal_Search search(depth,branch,false,&dark,gt);
  search.Goal_Value(state);
  oper=dark.Get_Operator();
}


Xarchon_Sequence::Xarchon_Sequence(int light_depth,int light_branch,int dark_depth,int dark_branch)
{
  light=new Xarchon_Light_Player(light_depth,light_branch);
  dark=new Xarchon_Dark_Player(dark_depth,dark_branch);
  warlord=new Xarchon_War_Lord;
}

Xarchon_Sequence::~Xarchon_Sequence(void)
{
  delete light;
  delete dark;
  delete warlord;
}

Strategy<double,Xarchon_State,int> *Xarchon_Sequence::Next_Strategy(Xarchon_State *p) 
{
  if (p->Fight())
    return warlord;
  if (p->Side()==0)
    return light;
  if (p->Side()==1)
    return dark;
  return warlord;
}

Xarchon_Goal_Search::Xarchon_Goal_Search(int d,int b,bool m,
					 Goal_Accumulator<double,Xarchon_State> *res,
					 Goal_Test<double,Xarchon_State> *t)
  : Successor_Goal_Test<double,Xarchon_State>(NULL,NULL,t)
{
  generator=new Xarchon_Op_Generator;
  max=m;
  goal_acc=res;
  depth=d;
  branch=b;
}

Xarchon_Goal_Search::~Xarchon_Goal_Search(void)
{
  delete generator;
}


double Xarchon_Goal_Search::Goal_Value(Xarchon_State *a)
{
#ifdef DEBUG_AI
   printf("Xarchon_Goal_Search::Goal_Value\n");
#endif   
  int endofgame=a->EndOfGame();
  if (endofgame!=0)
    return tester->Goal_Value(a);

  if (a->Fight()) {
    if (max) {
      Xarchon_Light_Accumulator light_win,light_lose;
      Xarchon_Goal_Search light_search_win (depth,branch,max,&light_win,tester);
      Xarchon_Goal_Search light_search_lose (depth,branch,max,&light_lose,tester);
      Xarchon_Fight_Goal_Test fighter(&light_search_win,&light_search_lose);
      return fighter.Goal_Value(a);
    }
    else {
      Xarchon_Dark_Accumulator dark_win,dark_lose;
      Xarchon_Goal_Search dark_search_win  (depth,branch,max,&dark_win,tester);
      Xarchon_Goal_Search dark_search_lose (depth,branch,max,&dark_lose,tester);
      Xarchon_Fight_Goal_Test fighter(&dark_search_win,&dark_search_lose);
      return fighter.Goal_Value(a);
    }
  }

  if (depth==0) 
    tester->Goal_Value(a);

  if (depth==1) 
    return Successor_Goal_Test<double,Xarchon_State>::Goal_Value(a);

  Goal_Accumulator<double,Xarchon_State> *old=goal_acc;

  if (max) {
    Multi_Goal_Accumulator<double,Xarchon_State,greater<double> > mga(branch);
    goal_acc=&mga;
    Successor_Goal_Test<double,Xarchon_State>::Goal_Value(a);
    goal_acc=old;
      for (mga.Init();!mga.IsEnd();mga.Next()) { 
      Xarchon_State *state=mga.Get_State();
      Operator<Xarchon_State> *oper=mga.Get_Operator();
#ifdef DEBUG_AI
      ((Xarchon_Operator *)oper)->Display();
#endif
      Xarchon_Dark_Accumulator dark;
      Xarchon_Goal_Search search(depth-1,branch,!max,&dark,tester);
      double new_value=search.Goal_Value(state);
#ifdef DEBUG_AI
      double old_value=tester->Goal_Value(state);
      printf ("(%g,%g) \n ",old_value,new_value);
#endif
      goal_acc->Put(state,oper,new_value);
    }
  }
  
  else {
    Multi_Goal_Accumulator<double,Xarchon_State,less<double> > mga(branch);
    goal_acc=&mga;

    Successor_Goal_Test<double,Xarchon_State>::Goal_Value(a);
    goal_acc=old;

    for (mga.Init();!mga.IsEnd();mga.Next()) { 
      Xarchon_State *state=mga.Get_State();
      Operator<Xarchon_State> *oper=mga.Get_Operator();
#ifdef DEBUG_AI
      ((Xarchon_Operator *)oper)->Display();
#endif
      Xarchon_Light_Accumulator light;
      Xarchon_Goal_Search search(depth-1,branch,!max,&light,tester);
      double new_value=search.Goal_Value(state);
#ifdef DEBUG_AI
      double old_value=tester->Goal_Value(state);
      printf ("(%g,%g) \n ",old_value,new_value);
#endif
      goal_acc->Put(state,oper,new_value);
    }
  }
#ifdef DEBUG_AI
  double val=goal_acc->Get_Value();
  printf ("Eval %g \n",val);
#endif
  return goal_acc->Get_Value();
}
