#ifndef XARCHON_PROBLEM_HPP
#define XARCHON_PROBLEM_HPP

extern "C" {
#include <config.h>
#include <sys/time.h>
#include "main.h"
#include "stdio.h"
#include "board.h"
#include "computer.h"
#include "field.h"
#include "actors.h"
#include "iface.h"
}

#include "Problem.hpp"

#include <list>

// < double , Xarchon_State , int >

typedef enum {
  MOVE_COMPLETE,
  NEED_ELEMENTAL,
  NEED_FIGHT
} Xarchon_Turn_Status;
  
class Xarchon_State : public State 
{
public:
  BOARD_STATE state;
  Xarchon_Turn_Status status;
  COMMAND cmd; // the command in construction
public:
  Xarchon_State(void) ;
  Xarchon_State(Xarchon_State *a) ;
  virtual ~Xarchon_State();
  void Destroy(void);
  void Get_Real_State(BOARD_STATE *real_state);
  void Start_Game(int game, int light_first);
  ACTOR *Actor_Copy(ACTOR *a);
  bool Fight(void);
  // wrappers for interface functions
  void Field_Statistics(double *p, int *a1, int *d1);
  // wrappers for board_rules functions
  int Height(void);
  int Width(void);
  int Side(void);
  int Turn(void);
  bool Empty(int x, int y);
  ACTOR *Actor(int x, int y);
  CELL *Cell(int x, int y);
  bool Power_Point(int x, int y);
  int Actor_Side(int x, int y);
  int EndOfGame(void);
  int Verify_Source(int x, int y);
  int Verify_Actor_Target(int ax0, int ay0, int x1, int y1);
  void Make_Move(COMMAND cmd);
  void Field_Done(bool win, int damage);
};

class Xarchon_Operator : public Operator <Xarchon_State> 
{
public:
  Xarchon_Operator(void) {}

  virtual ~Xarchon_Operator(void) {}
  virtual void Display(void) = 0;
};

class Xarchon_Move_Operator : public Xarchon_Operator
{
public:
  COMMAND cmd;
public:
  Xarchon_Move_Operator(int spell,int x2=0,int y2=0,int x1=0,int y1=0);
  virtual ~Xarchon_Move_Operator(void) {};
  virtual Xarchon_State* Operate(Xarchon_State *a) ;
  virtual void Display(void) 
    {
      printf ("(%d,%d) - (%d,%d) ",cmd.b.x1,cmd.b.y1,cmd.b.ax0,cmd.b.ay0);
    }
};

class Xarchon_Fight_Operator : public Xarchon_Operator
{
public:
  bool win;
  int damage;
public:
  Xarchon_Fight_Operator(bool w,int d);
  virtual ~Xarchon_Fight_Operator(void) {};
  virtual Xarchon_State *Operate(Xarchon_State *a) ;
  virtual void Display(void)
    {
      //        printf ("%s attacking %s ",attacker->name,defender->name);
    }
};

class Xarchon_Op_Generator : public Operator_Generator <Xarchon_State>
{
public:
  list <Operator <Xarchon_State> *> batch;
  list <Operator <Xarchon_State> *>::iterator batch_iter;
  virtual ~Xarchon_Op_Generator() ; 
public:
  Xarchon_Op_Generator(void);
  void Xarchon_Op_Generator::Generate_Moves(Xarchon_State *t);
  void Xarchon_Op_Generator::Generate_Actor_Targets(Xarchon_State *t, 
                                                    int ax0, int ay0);
  virtual void Init(Xarchon_State *t);
  virtual Operator<Xarchon_State> *Next(void)  ;
  virtual bool IsEnd(void) ;
};

enum {
  LIVE_SCORE,
  LIVE_HP_SCORE,
  POWERPOINT_SCORE,
  WIN_SCORE,
  RANDOM_SCORE,
  TOTAL_VALUE_PARMS
};

class Xarchon_Goal_Test : public Goal_Test <double,Xarchon_State>
{
public:
  double value_parm[TOTAL_VALUE_PARMS];
public:
  Xarchon_Goal_Test(void) ;
  virtual double Goal_Value(Xarchon_State *a) ;
};

class Xarchon_Fight_Goal_Test : public Goal_Test <double,Xarchon_State>
{
public:
  Goal_Test<double,Xarchon_State> *win;
  Goal_Test<double,Xarchon_State> *lose;
public:
  Xarchon_Fight_Goal_Test(Goal_Test <double,Xarchon_State> *w,Goal_Test <double, Xarchon_State> *l);
  virtual double Goal_Value(Xarchon_State *a);
};


class Xarchon_Path_Cost : public Path_Cost <Xarchon_State,int>
{
public:
  Xarchon_Path_Cost(void) {};
  virtual int Cost(Xarchon_State *a,Operator<Xarchon_State> *o) ;
};

class Xarchon_Problem : public Problem <double,Xarchon_State,int>
{
public:
  Xarchon_Problem(void);
  virtual ~Xarchon_Problem();
};

class Xarchon_Strategy     : public Strategy <double,Xarchon_State,int>
{
public:
  Operator<Xarchon_State> *oper;
public:
  Xarchon_Strategy(void);
  virtual ~Xarchon_Strategy(void);
  virtual Operator<Xarchon_State> *Next(void) ;
};

class Xarchon_War_Lord     : public Xarchon_Strategy
{
public:
  virtual void InitState(Xarchon_State *state) ;
};

class Xarchon_Player_Strategy : public Xarchon_Strategy
{
public:
  int depth;
  int branch;
public:
  Xarchon_Player_Strategy(int d,int b);
};

class Xarchon_Light_Player : public Xarchon_Player_Strategy
{
public:
  Xarchon_Goal_Test *gt;
public:
  Xarchon_Light_Player(int d,int b) ;
  ~Xarchon_Light_Player(void);
  virtual void InitState(Xarchon_State *state) ;
};

class Xarchon_Dark_Player  : public Xarchon_Player_Strategy
{
public:
  Xarchon_Goal_Test *gt;
public:
  Xarchon_Dark_Player(int d,int b);
  ~Xarchon_Dark_Player(void);
  virtual void InitState(Xarchon_State *state) ;
};


class Xarchon_Sequence : public Strategy_Sequence <double,Xarchon_State,int>
{
public:
  Xarchon_Light_Player *light;
  Xarchon_Dark_Player *dark;
  Xarchon_War_Lord *warlord;
public:
  Xarchon_Sequence(int light_depth,int light_branch,int dark_depth,int dark_branch);
  virtual ~Xarchon_Sequence(void);
  virtual Strategy<double,Xarchon_State,int> *Next_Strategy(Xarchon_State *p) ;
};

class Xarchon_Dark_Accumulator : public Min_Goal_Accumulator<double,Xarchon_State> {};
class Xarchon_Light_Accumulator : public Max_Goal_Accumulator<double,Xarchon_State> {};
class Xarchon_WarLord_Accumulator : public Goal_Accumulator<double,Xarchon_State> 
{
};

class Xarchon_Goal_Search : public Successor_Goal_Test<double,Xarchon_State>
{
public:
  bool max;
  int depth,branch;
public:
  Xarchon_Goal_Search(int depth,int branch,bool m,
		      Goal_Accumulator<double,Xarchon_State> *res,
		      Goal_Test<double,Xarchon_State> *end);
  virtual ~Xarchon_Goal_Search(void);
  
  virtual double Goal_Value(Xarchon_State *a);
};



#endif
